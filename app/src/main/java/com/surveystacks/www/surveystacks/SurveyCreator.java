package com.surveystacks.www.surveystacks;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ScrollView;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.Locale;

import retrofit.surveystacks.APIKeys;
import retrofit.surveystacks.APIService;
import retrofit.surveystacks.CustomToast;
import retrofit.surveystacks.RetroClient;
import retrofit.surveystacks.Session;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SurveyCreator extends BaseActivity {

    EditText et1, et2;
    Button btn1, btn2, btn3, btn4, btn5, btn6, btn7;
    CheckBox cb1;
    AlertDialog alertDialog1, alertDialog2, alertDialog3, alertDialog4, alertDialog5;
    Integer[] checkedRD = new Integer[]{};
    final ArrayList<String> selectedItemsRD = new ArrayList<>();
    ArrayList<String> categories = new ArrayList<>();
    ArrayList<String> catToken = new ArrayList<>();
    View v1, v2, v3;
    ScrollView scrollView;
    CustomToast toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrameLayout contentFrameLayout = findViewById(R.id.content_frame);
        getLayoutInflater().inflate(R.layout.survey_creator, contentFrameLayout);
        NavigationView navigationView = findViewById(R.id.nav_one_uui);
        setTitle("Survey Creator");

        fab1.setVisibility(View.GONE);

        et1 = findViewById(R.id.et_one_scd);
        et2 = findViewById(R.id.et_two_scd);
        //Button
        btn1 = findViewById(R.id.btn_one_scd);
        btn2 = findViewById(R.id.btn_two_scd);
        btn3 = findViewById(R.id.btn_three_scd);
        btn4 = findViewById(R.id.btn_four_scd);
        btn5 = findViewById(R.id.btn_five_scd);
        btn6 = findViewById(R.id.btn_six_scd);
        btn7 = findViewById(R.id.btn_seven_scd);
        //CheckBox
        cb1 = findViewById(R.id.cb_one_scd);
        // includes
        v1 = findViewById(R.id.include_one_sc);
        v2 = findViewById(R.id.include_two_sc);
        v3 = findViewById(R.id.include_three_sc);

        scrollView = findViewById(R.id.sv_one_creator);

        toast = new CustomToast();

        gatherData();
    }

    public void gatherData() {
        APIService apiService = RetroClient.getAPIService();

        Call<Session> call = apiService.creatorData(
                APIKeys.CREATOR_DATA[0],
                APIKeys.CREATOR_DATA[1],
                Locale.getDefault().getDisplayLanguage(),
                false
        );

        call.enqueue(new Callback<Session>() {
            @Override
            public void onResponse(@NonNull Call<Session> call, @NonNull Response<Session> response) {
                if (response.body().getResult_code() == 1) {
                    catToken.addAll(response.body().getCategories().keySet());
                    categories.addAll(response.body().getCategories().values());
                }
            }

            @Override
            public void onFailure(@NonNull Call<Session> call, @NonNull Throwable t) {

            }
        });
    }

    public void onRespondentDataClick(View view) {
        final String[] data = getResources().getStringArray(R.array.creator_automatic_data);

//        AlertDialog dialog = new AlertDialog.Builder(this)
//                .setTitle("Select Data")
//                .setMultiChoiceItems(data, checkedRD, new DialogInterface.OnMultiChoiceClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
//                        if (isChecked) {
//                            selectedItemsRD.add(data[which]);
//                        } else if (selectedItemsRD.contains(data[which])) {
//                            selectedItemsRD.remove(data[which]);
//                        }
//                        checkedRD[which] = isChecked;
//                    }
//                }).setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        StringBuilder stringBuilder = new StringBuilder();
//                        if (selectedItemsRD.size() > 0) {
//                            btn1.setTextColor(getResources().getColor(R.color.colorAccent));
//                            for (int i = 0; i < selectedItemsRD.size(); i++) {
//                                if (i+1 != selectedItemsRD.size()) {
//                                    stringBuilder = stringBuilder.append(selectedItemsRD.get(i)).append(", ");
//                                } else {
//                                    stringBuilder = stringBuilder.append(selectedItemsRD.get(i));
//                                }
//                            }
//                        } else {
//                            btn1.setTextColor(getResources().getColor(R.color.colorSemiDark));
//                            stringBuilder = stringBuilder.append("Automatic Respondent Data");
//                        }
//                        btn1.setText(stringBuilder.toString());
//                    }
//                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        alertDialog1.dismiss();
//                    }
//                }).create();
//        dialog.show();

        new MaterialDialog.Builder(this)
                .title("Select Data")
                .items(data)
                .itemsCallbackMultiChoice(checkedRD, new MaterialDialog.ListCallbackMultiChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, Integer[] which, CharSequence[] text) {
                        StringBuilder stringBuilder = new StringBuilder();
                        checkedRD = which;
                        if (text.length > 0) {
                            btn1.setTextColor(getResources().getColor(R.color.colorAccent));
                            for (int i = 0; i < text.length; i++) {

                                if (i == 3) {
                                    stringBuilder.append("...");
                                    continue;
                                } else if (i > 3) {
                                    continue;
                                }

                                if (i+1 != text.length) {
                                    stringBuilder = stringBuilder.append(text[i]).append(", ");
                                } else {
                                    stringBuilder = stringBuilder.append(text[i]);
                                }
                            }
                        } else {
                            btn1.setTextColor(getResources().getColor(R.color.colorSemiDark));
                            stringBuilder = stringBuilder.append("Automatic Respondent Data");
                        }
                        btn1.setText(stringBuilder.toString());
                        return true;
                    }
                })
                .positiveText("Add")
                .show();
    }

    public void onSurveyCategoryClick(View view) {
        new MaterialDialog.Builder(this)
                .title("Select Category)")
                .items(categories)
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        btn2.setText(categories.get(which));
                        btn2.setTextColor(getResources().getColor(R.color.colorAccent));
                        return true;
                    }
                })
                .typeface("opensansregular.ttf", "opensanslight.ttf")
                .alwaysCallSingleChoiceCallback()
                .show();
    }

    public void onTargetSexClick(View view) {
        final String[] sexes = getResources().getStringArray(R.array.sexes);

        new MaterialDialog.Builder(this)
                .title("Select Sex(s)")
                .items(sexes)
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        btn3.setText(sexes[which]);
                        btn3.setTextColor(getResources().getColor(R.color.colorAccent));
                        return true;
                    }
                })
                .typeface("opensansregular.ttf", "opensanslight.ttf")
                .alwaysCallSingleChoiceCallback()
                .show();
    }

    public void onTargetEthnicityClick(View view) {
        final String[] ethnicities = getResources().getStringArray(R.array.ethnicities);

        new MaterialDialog.Builder(this)
                .title("Select Ethnicity")
                .items(ethnicities)
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        btn4.setText(ethnicities[which]);
                        btn4.setTextColor(getResources().getColor(R.color.colorAccent));
                        return true;
                    }
                })
                .typeface("opensansregular.ttf", "opensanslight.ttf")
                .alwaysCallSingleChoiceCallback()
                .show();
    }

    public void onTargetAgeClick(View view) {
        final String[] ages = getResources().getStringArray(R.array.age_groups);
        new MaterialDialog.Builder(this)
                .title("Select Age Group")
                .items(ages)
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        btn5.setText(ages[which]);
                        btn5.setTextColor(getResources().getColor(R.color.colorAccent));
                        return true;
                    }
                })
                .typeface("opensansregular.ttf", "opensanslight.ttf")
                .alwaysCallSingleChoiceCallback()
                .show();
    }

    public void onSurveyImageClick(View view) {
        new MaterialDialog.Builder(this)
                .title("Select Image")
                .items(R.array.image_source)
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        if (which == 0) {
                            urlImage();
                        } else {
                            deviceImage();
                        }
                        return true;
                    }
                })
                .typeface("opensansregular.ttf", "opensanslight.ttf")
                .alwaysCallSingleChoiceCallback()
                .show();
    }

    public void urlImage() {
        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        final EditText edittext = new EditText(getApplicationContext());
        edittext.setHint("Enter the image url");
        alert.setTitle("Image URL");

        alert.setView(edittext);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                if (Patterns.WEB_URL.matcher(edittext.getText()).matches()) {
                  btn6.setText(edittext.getText());
                  btn6.setTextColor(getResources().getColor(R.color.colorAccent));
                }
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // what ever you want to do with No option.
            }
        });

        alert.show();
    }

    public void deviceImage() {
        Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
        getIntent.setType("image/*");

        Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("image/*");

        Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {pickIntent});

        startActivityForResult(chooserIntent, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == 1) {

        }
    }

    public void nextQuestions(View view) {
       v1.setVisibility(View.GONE);
       v2.setVisibility(View.VISIBLE);
       scrollView.fullScroll(ScrollView.FOCUS_UP);
    }

    public void previousDetails(View view) {
        v2.setVisibility(View.GONE);
        v1.setVisibility(View.VISIBLE);
        scrollView.fullScroll(ScrollView.FOCUS_UP);
    }

    public void nextPayment(View view) {
        v2.setVisibility(View.GONE);
        v3.setVisibility(View.VISIBLE);
        scrollView.fullScroll(ScrollView.FOCUS_UP);
    }

    public void previousQuestions(View view) {
        v3.setVisibility(View.GONE);
        v2.setVisibility(View.VISIBLE);
        scrollView.fullScroll(ScrollView.FOCUS_UP);
    }

    public void addQuestion(View view) {

    }
}
