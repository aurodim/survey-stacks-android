package com.surveystacks.www.surveystacks;

import android.support.design.widget.NavigationView;
import android.os.Bundle;
import android.widget.FrameLayout;

public class Redeem extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame);
        getLayoutInflater().inflate(R.layout.redeem, contentFrameLayout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_one_uui);
        navigationView.getMenu().getItem(5).setChecked(true);
        navigationView.getMenu().getItem(5).setEnabled(false);

        setTitle("Redeem");

    }
}
