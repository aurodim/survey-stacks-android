package com.surveystacks.www.surveystacks;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.Locale;

import retrofit.surveystacks.APIKeys;
import retrofit.surveystacks.APIService;
import retrofit.surveystacks.Globals;
import retrofit.surveystacks.RetroClient;
import retrofit.surveystacks.Session;
import retrofit.surveystacks.SharedPrefManager;
import retrofit.surveystacks.Survey;
import retrofit.surveystacks.SurveyAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Surveys extends BaseActivity {

    SharedPrefManager sharedPrefManager;
    Globals globals;
    Context context;
    ProgressBar pb1;
    ArrayList<Survey> surveyArrayList;
    SurveyAdapter surveysAdapter;
    RecyclerView rv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrameLayout contentFrameLayout = findViewById(R.id.content_frame);
        getLayoutInflater().inflate(R.layout.surveys, contentFrameLayout);
        NavigationView navigationView = findViewById(R.id.nav_one_uui);
        navigationView.getMenu().getItem(6).setChecked(true);
        navigationView.getMenu().getItem(6).setEnabled(false);

        setTitle("Surveys");

        context = getApplicationContext();
        sharedPrefManager = new SharedPrefManager(this);
        globals = new Globals();

        // component
        surveyArrayList = new ArrayList<>();
        pb1 = findViewById(R.id.pb_one_s);
        rv1 = findViewById(R.id.rv_one_s);
        rv1.setHasFixedSize(true);
        rv1.setLayoutManager(new LinearLayoutManager(this));
        rv1.setItemViewCacheSize(20);
        rv1.setDrawingCacheEnabled(true);
        rv1.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        gatherSurveys();
    }

    public void gatherSurveys() {
        APIService apiService = RetroClient.getAPIService();

        Call<Session> call = apiService.surveysData(
                APIKeys.SURVEYS_DATA[0],
                APIKeys.SURVEYS_DATA[1],
                sharedPrefManager.getVK(),
                sharedPrefManager.getAK(),
                sharedPrefManager.getSK(),
                Locale.getDefault().getDisplayLanguage(),
                false
        );

        call.enqueue(new Callback<Session>() {
            @Override
            public void onResponse(@NonNull Call<Session> call, @NonNull Response<Session> response) {
                int resultCode = response.body().getResult_code();

                surveyArrayList = response.body().getSurveys();
                surveysAdapter = new SurveyAdapter(Surveys.this, surveyArrayList);
                rv1.setAdapter(surveysAdapter);
                pb1.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<Session> call, @NonNull Throwable t) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

}