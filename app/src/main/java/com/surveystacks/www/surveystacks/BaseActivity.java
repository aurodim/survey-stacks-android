package com.surveystacks.www.surveystacks;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import java.util.Locale;
import retrofit.surveystacks.APIKeys;
import retrofit.surveystacks.APIService;
import retrofit.surveystacks.Globals;
import retrofit.surveystacks.RetroClient;
import retrofit.surveystacks.Session;
import retrofit.surveystacks.SharedPrefManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BaseActivity extends AppCompatActivity {
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle actionBarDrawerToggle;
    Toolbar toolbar;
    FloatingActionButton fab1, fab2, fab3, fab4;
    Animation FabOpen, FabClose, FabClockwise, FabCounterClockwise;
    SharedPrefManager sharedPrefManager;
    ImageView imb1;
    TextView tv1, tv2, tv3, tv4;
    Globals globals;
    boolean isOpen = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        NavigationView navigationView = findViewById(R.id.nav_one_uui);

        View headerView = navigationView.getHeaderView(0);
        imb1 = headerView.findViewById(R.id.imb_one_uui);
        tv1 = headerView.findViewById(R.id.tv_one_uui);
        tv2 = headerView.findViewById(R.id.tv_two_uui);
        tv3 = headerView.findViewById(R.id.tv_three_uui);
        tv4 = findViewById(R.id.toolbar_title);

        //Floating Action Button
        fab1 = findViewById(R.id.fab_one_uui);
        fab2 = findViewById(R.id.fab_two_uui);
        fab3 = findViewById(R.id.fab_three_uui);
        fab4 = findViewById(R.id.fab_four_uui);

        FabOpen = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        FabClose = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_close);
        FabClockwise = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_clockwise);
        FabCounterClockwise = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_counterclockwise);

        sharedPrefManager = new SharedPrefManager(getApplicationContext());
        globals = new Globals();

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getActionBar();
        getSupportActionBar();

        drawerLayout = findViewById(R.id.drawer_layout);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);


        Glide.with(getApplicationContext()).load(sharedPrefManager.getKeyPfp()).into(imb1);
        tv1.setText(sharedPrefManager.getUsername());
        tv2.setText(String.valueOf(sharedPrefManager.getKEY_Stacks()));
        tv3.setText(String.valueOf(sharedPrefManager.getKEY_Coins()));

        navigationView.setNavigationItemSelectedListener
                (new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        final String appPackageName = getPackageName();

                        switch (item.getItemId()) {
                            case R.id.nav_analytics:
                                startActivity(new Intent(getApplicationContext(), Analytics.class));
                                drawerLayout.closeDrawers();
                                break;
                            case R.id.nav_bookmarks:
                                startActivity(new Intent(getApplicationContext(), Bookmarks.class));
                                drawerLayout.closeDrawers();
                                break;
                            case R.id.nav_cts:
                                // coins to stacks
                                break;
                            case R.id.nav_logout:
                                sharedPrefManager.logout();
                                startActivity(new Intent(getApplicationContext(), Login.class));
                                drawerLayout.closeDrawers();
                                break;
                            case R.id.nav_manage:
                                startActivity(new Intent(getApplicationContext(), Manager.class));
                                drawerLayout.closeDrawers();
                                break;
                            case R.id.nav_polls:
                                startActivity(new Intent(getApplicationContext(), Polls.class));
                                drawerLayout.closeDrawers();
                                break;
                            case R.id.nav_profile:
                                startActivity(new Intent(getApplicationContext(), Profile.class));
                                drawerLayout.closeDrawers();
                                break;
                            case R.id.nav_redeem:
                                startActivity(new Intent(getApplicationContext(), Redeem.class));
                                drawerLayout.closeDrawers();
                                break;
                            case R.id.nav_settings:
                                startActivity(new Intent(getApplicationContext(), Settings.class));
                                drawerLayout.closeDrawers();
                                break;
                            case R.id.nav_surveys:
                                startActivity(new Intent(getApplicationContext(), Surveys.class));
                                drawerLayout.closeDrawers();
                                break;
                            case R.id.nav_teams:
                                startActivity(new Intent(getApplicationContext(), Teams.class));
                                drawerLayout.closeDrawers();
                                break;
                        }

                        return false;
                    }
                });

        if (!sharedPrefManager.getSessionInit()) {
            gatherSessionData();
        }
    }

    public void gatherSessionData() {

        APIService apiService = RetroClient.getAPIService();

        Call<Session> call = apiService.sessionData(
                APIKeys.SESSION_DATA[0],
                APIKeys.SESSION_DATA[1],
                sharedPrefManager.getVK(),
                sharedPrefManager.getAK(),
                sharedPrefManager.getSK(),
                Locale.getDefault().getDisplayLanguage(),
                false
        );

        call.enqueue(new Callback<Session>() {
            @Override
            public void onResponse(@NonNull Call<Session> call, @NonNull Response<Session> response) {
                int resultCode = response.body().getResult_code();
                sharedPrefManager.setKeyInit(true);
                if (resultCode == 1) {
                    personalizeView(response.body());
                } else if (resultCode == -1) {
                    // so on
                }
            }

            @Override
            public void onFailure(@NonNull Call<Session> call, @NonNull Throwable t) {

            }
        });
    }

    public void personalizeView(Session response) {
        String pfp_url = globals.imageTransformation(response.accountInfoSession().getProfile_picture_url(), 75, 75, "c_scale,r_max", getApplicationContext(), true);

        sharedPrefManager.setCSRF(response.getCsrf_token());
        sharedPrefManager.setKeyPfp(pfp_url);
        sharedPrefManager.setKeyPtp(response.accountInfoSession().getProfile_theme_url());
        sharedPrefManager.setKEY_Stacks(response.accountInfoSession().getStacks());
        sharedPrefManager.setKEY_Coins(response.accountInfoSession().getCoins());

        Glide.with(getApplicationContext()).load(pfp_url).into(imb1);
        tv2.setText(String.valueOf(response.accountInfoSession().getStacks()));
        tv3.setText(String.valueOf(response.accountInfoSession().getCoins()));
    }

    public void onFabOnePlusClick(View view){
        if (isOpen) {
            fab2.startAnimation(FabClose);
            fab3.startAnimation(FabClose);
            fab4.startAnimation(FabClose);
            fab1.startAnimation(FabCounterClockwise);
            fab2.setClickable(false);
            fab3.setClickable(false);
            fab4.setClickable(false);
            isOpen = false;
        } else {
            fab2.startAnimation(FabOpen);
            fab3.startAnimation(FabOpen);
            fab4.startAnimation(FabOpen);
            fab1.startAnimation(FabClockwise);
            fab2.setClickable(true);
            fab3.setClickable(true );
            fab4.setClickable(true);
            isOpen = true;
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        actionBarDrawerToggle.syncState();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransitionExit();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();
    }

    /**
     * Overrides the pending Activity transition by performing the "Enter" animation.
     */
    protected void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    /**
     * Overrides the pending Activity transition by performing the "Exit" animation.
     */
    protected void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_left);
    }

    public void OpenLabs(View view) {
        startActivity(new Intent(getApplicationContext(), Labs.class));
    }

    public void OpenSurveyCreator(View view) {
        startActivity(new Intent(getApplicationContext(), SurveyCreator.class));
    }

    public void OpenPollCreator(View view) {
        startActivity(new Intent(getApplicationContext(), PollCreatorDetails.class));
    }
}
