package com.surveystacks.www.surveystacks;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.icu.text.SimpleDateFormat;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.GsonBuilder;

import java.util.Calendar;
import java.util.Locale;

import retrofit.surveystacks.APIService;
import retrofit.surveystacks.APIKeys;
import retrofit.surveystacks.CustomToast;
import retrofit.surveystacks.RetroClient;
import retrofit.surveystacks.SessionInit;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotUsername extends AppCompatActivity {

    TextView tv1, tv2, tv3, tv4;
    EditText et1, et2;
    Button btn1;
    ImageButton imb1;
    CardView cv1;

    Calendar myCalendar = Calendar.getInstance();
    Context context;
    CustomToast toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_username);

        //TextViews
        tv1 = findViewById(R.id.tv_one_fu);
        tv2 = findViewById(R.id.tv_two_fu);
        tv3 = findViewById(R.id.tv_three_fu);
        tv4 = findViewById(R.id.tv_four_fu);
        //tv1.setText(response);

        //EditTexts
        et1 = findViewById(R.id.et_one_fu);
        et2 = findViewById(R.id.et_two_fu);
        //Buttons
        btn1 = findViewById(R.id.btn_one_fu);
        //ImageButtons
        imb1 = findViewById(R.id.imb_one_fu);
        //CardViews
        cv1 = findViewById(R.id.cv_one_fu);
        //Transitions
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
        //Calendar
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, month);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };
        //mm-dd-yyyy format
        et2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(ForgotUsername.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    private void updateLabel() {
        String myFormat = "MM-dd-YYYY";
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
            et2.setText(sdf.format(myCalendar.getTime()));
            Log.i("date", sdf.format(myCalendar.getTime()));
        }
    }

    @Override
    public void onBackPressed() {
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
        super.onBackPressed();
    }

    public void onRecoverUsernameClick (View view) {

        //Progress Dialog
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("SurveyStacks");
        progressDialog.setMessage("Recovering Username...");
        progressDialog.show();

        //String (EditText)
        String email_phoneNumber = et1.getText().toString().trim();
        String dob = et2.getText().toString().trim();

        //Calling API controller
        APIService apiService = RetroClient.getAPIService();

        //Session Init
        Call<SessionInit> call = apiService.forgot_username(
                APIKeys.FORGOT_USERNAME,
                "en",
                email_phoneNumber,
                dob,
                false
        );

        //Request
        call.enqueue(new Callback<SessionInit>() {
            @Override
            public void onResponse(@NonNull Call<SessionInit> call, @NonNull Response<SessionInit> response) {
                progressDialog.dismiss();
                response.body();
                Log.i("r", new GsonBuilder().setPrettyPrinting().create().toJson(response.body()));
                switch (response.body().getResult_code()) {
                    case 1:
//                        tv3.setText(response.body().getUsername());
                        Toast.makeText(ForgotUsername.this, "success", Toast.LENGTH_SHORT).show();
                        break;
                    case -1:
                        break;
                    case -2:
                        break;
                    case -3:
                        break;
                }
            }

            @Override
            public void onFailure(@NonNull Call<SessionInit> call, @NonNull Throwable t) {
                Toast.makeText(ForgotUsername.this, "Login Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
