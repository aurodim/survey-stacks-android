package com.surveystacks.www.surveystacks;

import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.os.Bundle;
import android.widget.FrameLayout;

import java.util.Locale;

import retrofit.surveystacks.APIKeys;
import retrofit.surveystacks.APIService;
import retrofit.surveystacks.RetroClient;
import retrofit.surveystacks.Session;
import retrofit.surveystacks.SharedPrefManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Analytics extends BaseActivity {

    SharedPrefManager sharedPrefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame);
        getLayoutInflater().inflate(R.layout.analytics, contentFrameLayout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_one_uui);
        navigationView.getMenu().getItem(0).setChecked(true);
        navigationView.getMenu().getItem(0).setEnabled(false);

        setTitle("Analytics");

        sharedPrefManager = new SharedPrefManager(getApplicationContext());

        //gatherAnalytics();
    }

    public void gatherAnalytics() {
        APIService apiService = RetroClient.getAPIService();

        Call<Session> call = apiService.analyticsData(
                APIKeys.ANALYTICS_DATA[0],
                APIKeys.ANALYTICS_DATA[1],
                sharedPrefManager.getVK(),
                sharedPrefManager.getAK(),
                sharedPrefManager.getSK(),
                Locale.getDefault().getDisplayLanguage(),
                false
        );

        call.enqueue(new Callback<Session>() {
            @Override
            public void onResponse(@NonNull Call<Session> call, @NonNull Response<Session> response) {

            }

            @Override
            public void onFailure(@NonNull Call<Session> call, @NonNull Throwable t) {

            }
        });
    }
}
