package com.surveystacks.www.surveystacks;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.Locale;
import java.util.Objects;

import retrofit.surveystacks.APIKeys;
import retrofit.surveystacks.APIService;
import retrofit.surveystacks.Globals;
import retrofit.surveystacks.Profile;
import retrofit.surveystacks.RetroClient;
import retrofit.surveystacks.SharedPrefManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Profile extends BaseActivity {

    Globals globals;
    SharedPrefManager sharedPrefManager;
    ImageButton imb1, imb2, imb3, imb4, imb5, imb6;
    ImageView imv1;
    String fb, ig, web, email, phone;
    TextView tv1, tv2, tv3, tv4, tv5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrameLayout contentFrameLayout = findViewById(R.id.content_frame);
        getLayoutInflater().inflate(R.layout.profile, contentFrameLayout);
        NavigationView navigationView = findViewById(R.id.nav_one_uui);
        navigationView.getMenu().getItem(2).setChecked(true);
        navigationView.getMenu().getItem(2).setEnabled(false);

        setTitle("Profile");

        globals = new Globals();
        sharedPrefManager = new SharedPrefManager(getApplicationContext());
        imb1 = findViewById(R.id.imb_one_p);
        imb2 = findViewById(R.id.imb_two_p);
        imb3 = findViewById(R.id.imb_three_p);
        imb4 = findViewById(R.id.imb_four_p);
        imb5 = findViewById(R.id.imb_five_p);
        imb6 = findViewById(R.id.imb_six_p);
        imv1 = findViewById(R.id.imv_one_p);
        tv1 = findViewById(R.id.tv_one_p);
        tv2 = findViewById(R.id.tv_two_p);
        tv3 = findViewById(R.id.tv_three_p);
        tv4 = findViewById(R.id.tv_four_p);
        tv5 = findViewById(R.id.tv_five_p);

        gatherProfileData();
    }

    public void gatherProfileData() {
        APIService apiService = RetroClient.getAPIService();

        Call<retrofit.surveystacks.Profile> call = apiService.profileData(
                APIKeys.PROFILE_DATA[0],
                APIKeys.PROFILE_DATA[1],
                sharedPrefManager.getVK(),
                sharedPrefManager.getAK(),
                sharedPrefManager.getSK(),
                Locale.getDefault().getDisplayLanguage(),
                false
        );

        call.enqueue(new Callback<retrofit.surveystacks.Profile>() {
            @Override
            public void onResponse(@NonNull Call<retrofit.surveystacks.Profile> call, @NonNull Response<retrofit.surveystacks.Profile> response) {
                int resultCode = response.body().getResult_code();
                if (resultCode == 1) {
                    bindProfile(response.body());
                } else if (resultCode == -1) {
                    // so on
                }
            }

            @Override
            public void onFailure(@NonNull Call<retrofit.surveystacks.Profile> call, @NonNull Throwable t) {

            }
        });
    }

    public void bindProfile(retrofit.surveystacks.Profile response) {
        String pfp_url = globals.imageTransformation(response.accountInfoProfile().getProfile_picture_url(), 100, 100, "c_scale,r_max", getApplicationContext(), true);
        String ptp_url = globals.imageTransformation(response.accountInfoProfile().getProfile_theme_url(), 80, 80, "c_scale,r_max", getApplicationContext(), true);
        Glide.with(getApplicationContext()).load(ptp_url).into(imb1);
        Glide.with(getApplicationContext()).load(pfp_url).into(imv1);

        tv1.setText(sharedPrefManager.getUsername());
        tv2.setText(response.accountInfoProfile().getMotto());
        tv3.setText(response.achievementsInfoProfile().getLifetime_stacks(getApplicationContext()));
        tv4.setText(response.achievementsInfoProfile().getLifetime_coins(getApplicationContext()));
        tv5.setText(response.accountInfoProfile().getMember_since(getApplicationContext()));

        fb = response.contactInfoProfile().getFacebook();
        ig = response.contactInfoProfile().getInstagram();
        web = response.contactInfoProfile().getWebsite();
        email = response.contactInfoProfile().getEmail();
        phone = response.contactInfoProfile().getPhone_number();

        if (!Objects.equals(response.accountInfoProfile().getMotto(), "")) {
            tv2.setVisibility(View.VISIBLE);
        }

        if (!Objects.equals(response.achievementsInfoProfile().getLifetime_stacks(getApplicationContext()), "")) {
            tv3.setVisibility(View.VISIBLE);
        }

        if (!Objects.equals(response.achievementsInfoProfile().getLifetime_coins(getApplicationContext()), "")) {
            tv4.setVisibility(View.VISIBLE);
        }

        if (!Objects.equals(fb, "")) {
            imb2.setVisibility(View.VISIBLE);
        }

        if (!Objects.equals(ig, "")) {
            imb3.setVisibility(View.VISIBLE);
        }

        if (!Objects.equals(web, "")) {
            imb4.setVisibility(View.VISIBLE);
        }

        if (!Objects.equals(email, "")) {
            imb5.setVisibility(View.VISIBLE);
        }

        if (!Objects.equals(phone, "")) {
            imb6.setVisibility(View.VISIBLE);
        }
    }

    public void onPfpClick(View view) {
    }

    public void onThemePicClick(View view) {
    }

    public void onFacebookIconClick(View view) {
        Intent fbIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("https://www.facebook.com/"+fb));
        startActivity(fbIntent);
    }

    public void onInstagramIconClick(View view) {
        Intent igIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("https://www.instargram.com/"+ig));
        startActivity(igIntent);
    }

    public void onWebsiteIconClick(View view) {
        Intent igIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse(web));
        startActivity(igIntent);
    }

    public void onEmailIconClick(View view) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setData(Uri.parse("mailto:"+email));
        startActivity(emailIntent);
    }

    public void onPhoneIconClick(View view) {
        Uri uri = Uri.parse("smsto:"+phone);
        Intent phoneIntent = new Intent(Intent.ACTION_SENDTO, uri);
        startActivity(phoneIntent);
    }
}
