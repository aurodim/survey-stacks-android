package com.surveystacks.www.surveystacks;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import retrofit.surveystacks.BookmarkAdapter;
import retrofit.surveystacks.Globals;
import retrofit.surveystacks.SharedPrefManager;
import retrofit.surveystacks.Survey;

public class SurveysFragment extends Fragment {

    SharedPrefManager sharedPrefManager;
    Globals globals;
    Context context;
    ArrayList<Survey> surveyArrayList;
    BookmarkAdapter surveysAdapter;
    RecyclerView rv1;
    FragmentActivity listener;
    View view;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity){
            this.listener = (FragmentActivity) context;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.surveys_fragment, parent, false);
        context = getActivity().getApplicationContext();
        sharedPrefManager = new SharedPrefManager(getActivity());
        globals = new Globals();
        surveyArrayList = new ArrayList<>();
        rv1 = view.findViewById(R.id.rv_one_b);
        rv1.setHasFixedSize(true);
        rv1.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv1.setItemViewCacheSize(20);
        rv1.setDrawingCacheEnabled(true);
        rv1.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.listener = null;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void bindData(ArrayList<Survey> data) {
        surveyArrayList = data;
        surveysAdapter = new BookmarkAdapter(getActivity(), surveyArrayList, null, 0);
        surveysAdapter.notifyDataSetChanged();
        rv1.setAdapter(surveysAdapter);
    }
}