package com.surveystacks.www.surveystacks;

import android.app.ProgressDialog;
import android.support.design.widget.NavigationView;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Switch;
import android.widget.TextView;

import java.util.Locale;

import retrofit.surveystacks.APIKeys;
import retrofit.surveystacks.APIService;
import retrofit.surveystacks.RetroClient;
import retrofit.surveystacks.Settings;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Settings extends BaseActivity {

    ProgressDialog progressDialog;
    TextView tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10, tv11, tv12, tv13, tv15, tv16, tv17, tv18, tv19, tv20, tv21, tv22, tv23, tv24, tv25, tv26;
    EditText et1, et2, et3, et4, et5, et6, et7, et8;
    Switch sw1, sw2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrameLayout contentFrameLayout = findViewById(R.id.content_frame);
        getLayoutInflater().inflate(R.layout.settings, contentFrameLayout);
        NavigationView navigationView = findViewById(R.id.nav_one_uui);
        navigationView.getMenu().getItem(8).setChecked(true);
        navigationView.getMenu().getItem(8).setEnabled(false);

        setTitle("Settings");

        tv1 = findViewById(R.id.tv_two_settings);
        tv2 = findViewById(R.id.tv_three_settings);
        tv3 = findViewById(R.id.tv_four_settings);
        tv4 = findViewById(R.id.tv_five_settings);
        tv5 = findViewById(R.id.tv_six_settings);
        tv6 = findViewById(R.id.tv_ten_b_settings);
        tv7 = findViewById(R.id.tv_eight_settings);
        tv8 = findViewById(R.id.tv_nine_settings);
        tv9 = findViewById(R.id.tv_ten_settings);
        tv10 = findViewById(R.id.tv_eleven_settings);
        tv11 = findViewById(R.id.tv_twelve_settings);
        tv12 = findViewById(R.id.tv_thirteen_settings);
        tv13 = findViewById(R.id.tv_fourteen_settings);
        tv15 = findViewById(R.id.tv_sixteen_settings);
        tv16 = findViewById(R.id.tv_seventeen_settings);
        tv17 = findViewById(R.id.tv_eighteen_settings);
        tv18 = findViewById(R.id.tv_nineteen_settings);
        tv19 = findViewById(R.id.tv_twenty_settings);
        tv20 = findViewById(R.id.tv_twentyone_settings);
        tv21 = findViewById(R.id.tv_twentytwo_settings);
        tv22 = findViewById(R.id.tv_twentythree_settings);
        tv23 = findViewById(R.id.tv_twentyfour_settings);
        tv24 = findViewById(R.id.tv_twentyfive_settings);
        tv25 = findViewById(R.id.tv_twentysix_settings);
        tv26 = findViewById(R.id.tv_twentyseven_settings);

        et1 = findViewById(R.id.et_one_settings);
        et2 = findViewById(R.id.et_two_settings);
        et3 = findViewById(R.id.et_three_settings);
        et4 = findViewById(R.id.et_four_settings);
        et5 = findViewById(R.id.et_five_settings);
        et6 = findViewById(R.id.et_six_settings);
        et7 = findViewById(R.id.et_seven_settings);
        et8 = findViewById(R.id.et_eight_settings);

        sw1 = findViewById(R.id.sw_one_settings);
        sw2 = findViewById(R.id.sw_two_settings);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

        gatherSettingsData();
    }

    public void gatherSettingsData() {
        APIService apiService = RetroClient.getAPIService();

        Call<retrofit.surveystacks.Settings> call = apiService.settingsData(
                APIKeys.SETTINGS_DATA[0],
                APIKeys.SETTINGS_DATA[1],
                sharedPrefManager.getVK(),
                sharedPrefManager.getAK(),
                sharedPrefManager.getSK(),
                Locale.getDefault().getDisplayLanguage(),
                false
        );

        progressDialog.show();

        call.enqueue(new Callback<retrofit.surveystacks.Settings>() {
            @Override
            public void onResponse(Call<retrofit.surveystacks.Settings> call, Response<retrofit.surveystacks.Settings> response) {
                int resultCode = response.body().getResult_code();
                retrofit.surveystacks.Settings.AccountInfoSettings accountInfoSettings = response.body().getAccountInfoSettings();
                retrofit.surveystacks.Settings.AboutInfoSettings aboutInfoSettings = response.body().getAboutInfoSettings();
                retrofit.surveystacks.Settings.ContactInfoSettings contactInfoSettings = response.body().getContactInfoSettings();
                retrofit.surveystacks.Settings.NotificationsInfoSettings notificationsInfo = response.body().getNotificationsInfo();
                retrofit.surveystacks.Settings.PaymentInfoSettings paymentInfoSettings = response.body().getPaymentInfoSettings();
                retrofit.surveystacks.Settings.PreferencesInfoSettings preferencesInfoSettings = response.body().getPreferencesInfoSettings();

                et1.setText(accountInfoSettings.getEmailPhone());
                et2.setText(contactInfoSettings.getPhone_number());
                et3.setText(contactInfoSettings.getEmail());
                et4.setText(contactInfoSettings.getWebsite());
                et5.setText(contactInfoSettings.getFacebook());
                et6.setText(contactInfoSettings.getInstagram());

                tv6.setText(aboutInfoSettings.getEducation(getApplicationContext()));
                tv7.setText(aboutInfoSettings.getSex(getApplicationContext()));
                tv8.setText(aboutInfoSettings.getEthnicity(getApplicationContext()));
                tv9.setText(aboutInfoSettings.getLang(getApplicationContext()));
                tv10.setText(aboutInfoSettings.getRelationship(getApplicationContext()));
                tv11.setText(aboutInfoSettings.getEmployment(getApplicationContext()));
                tv12.setText(aboutInfoSettings.getReligion(getApplicationContext()));

                tv15.setText(preferencesInfoSettings.getSex(getApplicationContext()));
                tv16.setText(preferencesInfoSettings.getTranslated(getApplicationContext()));
                tv17.setText(preferencesInfoSettings.getCategories(getApplicationContext()));
                tv18.setText(preferencesInfoSettings.getCompanies(getApplicationContext()));
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<retrofit.surveystacks.Settings> call, Throwable t) {

            }
        });
    }

    public void onUpdateAccountInfo(View view) {
    }

    public void onUpdateContactInfo(View view) {
    }

    public void onUpdatePassword(View view) {
    }

    public void onUpdateNotificationSettings(View view) {
    }

    public void onUpdateAboutMe(View view) {
    }

    public void onUpdatePreferences(View view) {
    }

    public void onTermsAndConditionsClick(View view) {
    }

    public void onCookiesPolicyClick(View view) {
    }

    public void onForumsClick(View view) {
    }

    public void onContactUsClick(View view) {
    }

    public void onDeactivateAccountClick(View view) {
    }
}
