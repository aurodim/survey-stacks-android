package com.surveystacks.www.surveystacks;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

import retrofit.surveystacks.APIService;
import retrofit.surveystacks.APIKeys;
import retrofit.surveystacks.CustomToast;
import retrofit.surveystacks.RetroClient;
import retrofit.surveystacks.SessionInit;
import retrofit.surveystacks.SharedPrefManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity {

    TextView tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8;
    EditText et1, et2;
    Button btn1;
    RelativeLayout rl1, rl2;
    CardView cv1;
    Context context;
    CustomToast toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        //TextViews
        tv1 = findViewById(R.id.tv_one_login);
        tv2 = findViewById(R.id.tv_two_login);
        tv3 = findViewById(R.id.tv_three_login);
        tv4 = findViewById(R.id.tv_four_login);
        tv5 = findViewById(R.id.tv_five_login);
        tv6 = findViewById(R.id.tv_six_login);
        tv7 = findViewById(R.id.tv_seven_login);
        tv8 = findViewById(R.id.tv_eight_login);
        //EditTexts
        et1 = findViewById(R.id.et_one_login);
        et2 = findViewById(R.id.et_two_login);
        //Buttons
        btn1 = findViewById(R.id.btn_one_login);
        //RelativeLayouts
        rl1 = findViewById(R.id.rl_one_login);
        rl2 = findViewById(R.id.rl_two_login);
        //CardViews
        cv1 = findViewById(R.id.cv_one_login);
        //Transition
        //overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    public void onLoginClick (View view) {
        //progress dialog
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("SurveyStacks");
        progressDialog.setMessage("Logging In...");
        progressDialog.show();

        //String (EditText)
        String username = et1.getText().toString().trim();
        String password = et2.getText().toString().trim();

        APIService apiService = RetroClient.getAPIService();

        Call<SessionInit> call = apiService.login(
                APIKeys.LOGIN,
                Locale.getDefault().getDisplayLanguage(),
                username,
                password,
                false
        );

        call.enqueue(new Callback<SessionInit>() {
            @Override
            public void onResponse(Call<SessionInit> call, Response<SessionInit> response) {
                progressDialog.dismiss();
                int resultCode = response.body().getResult_code();
                if (resultCode == 1) {
                    SharedPrefManager.getInstance(getApplicationContext())
                                .storeSession(response.body().getProfileAuthData().getUsername(),
                                        response.body().getProfileAuthData().getSession_key(),
                                        response.body().getProfileAuthData().getAuth_key(),
                                        response.body().getProfileAuthData().getValidation_key(),
                                        response.body().getProfileAuthData().getSetup_completed());

                    startActivity(new Intent(getApplicationContext(), BaseActivity.class));
                    Toast.makeText(Login.this, "success", Toast.LENGTH_SHORT).show();
                } else if (resultCode == -1) {
                    // so on
                }
            }

            @Override
            public void onFailure(Call<SessionInit> call, Throwable t) {

            }
        });
    }

    public void onForgotPasswordClick (View view) {
        Intent i = new Intent(this, ForgotPass.class);
        startActivity(i);
    }

    public void onForgotUsernameClick (View view) {
        Intent i = new Intent(this, ForgotUsername.class);
        startActivity(i);
    }

    public void onSignUpClick (View view) {
        Intent i = new Intent(this, Register.class);
        startActivity(i);
    }
}
