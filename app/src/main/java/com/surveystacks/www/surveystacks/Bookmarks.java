package com.surveystacks.www.surveystacks;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.FrameLayout;

import java.util.ArrayList;
import java.util.Locale;

import retrofit.surveystacks.APIKeys;
import retrofit.surveystacks.APIService;
import retrofit.surveystacks.Poll;
import retrofit.surveystacks.RetroClient;
import retrofit.surveystacks.Session;
import retrofit.surveystacks.SharedPrefManager;
import retrofit.surveystacks.Survey;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Bookmarks extends BaseActivity {

    SharedPrefManager sharedPrefManager;
    ArrayList<Survey> surveyArrayList;
    ArrayList<Poll> pollArrayList;
    int currentFrag;
    SurveysFragment surveysFragment;
    PollsFragment pollsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrameLayout contentFrameLayout = findViewById(R.id.content_frame);
        getLayoutInflater().inflate(R.layout.bookmarks, contentFrameLayout);
        NavigationView navigationView = findViewById(R.id.nav_one_uui);
        navigationView.getMenu().getItem(1).setChecked(true);
        navigationView.getMenu().getItem(1).setEnabled(false);
        setTitle("Bookmarks");

        currentFrag = 0;
        surveysFragment = new SurveysFragment();
        sharedPrefManager = new SharedPrefManager(getApplicationContext());
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.bookmarks_fl, surveysFragment);
        ft.commit();

        final BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bnv_bookmarks);
        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        switch (item.getItemId()) {
                            case R.id.mi_s:
                                currentFrag = 0;
                                surveysFragment = new SurveysFragment();
                                bottomNavigationView.getMenu().getItem(0).setChecked(true);
                                transaction.replace(R.id.bookmarks_fl, surveysFragment).addToBackStack(null);
                                break;
                            case R.id.mi_p:
                                currentFrag = 1;
                                pollsFragment = new PollsFragment();
                                bottomNavigationView.getMenu().getItem(1).setChecked(true);
                                transaction.replace(R.id.bookmarks_fl, pollsFragment).addToBackStack(null);;
                                break;
                        }

                        transaction.commit();
                        emitData();
                        return true;
                    }
                });

        bottomNavigationView.getMenu().getItem(0).setChecked(true);

        gatherBookmarks();
    }

    public void gatherBookmarks() {
        APIService apiService = RetroClient.getAPIService();

        Call<Session> call = apiService.bookmarksData(
                APIKeys.BOOKMARKS_DATA[0],
                APIKeys.BOOKMARKS_DATA[1],
                sharedPrefManager.getVK(),
                sharedPrefManager.getAK(),
                sharedPrefManager.getSK(),
                Locale.getDefault().getDisplayLanguage(),
                false
        );

        call.enqueue(new Callback<Session>() {
            @Override
            public void onResponse(@NonNull Call<Session> call, @NonNull Response<Session> response) {
                int resultCode = response.body().getResult_code();

                if (resultCode == 1) {
                    surveyArrayList = response.body().getSurveysBookmarks();
                    pollArrayList = response.body().getPollsBookmarks();

                    emitData();
                }
            }

            @Override
            public void onFailure(@NonNull Call<Session> call, @NonNull Throwable t) {

            }
        });
    }

    public void emitData() {
        getSupportFragmentManager().executePendingTransactions();
        if (currentFrag == 0) {
            surveysFragment.bindData(surveyArrayList);
        } else {
            pollsFragment.bindData(pollArrayList);
        }
    }
}
