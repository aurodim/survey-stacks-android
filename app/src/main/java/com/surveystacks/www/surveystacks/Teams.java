package com.surveystacks.www.surveystacks;

import android.content.Context;
import android.support.design.widget.NavigationView;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

import retrofit.surveystacks.APIKeys;
import retrofit.surveystacks.APIService;
import retrofit.surveystacks.Globals;
import retrofit.surveystacks.RetroClient;
import retrofit.surveystacks.Session;
import retrofit.surveystacks.SharedPrefManager;
import retrofit.surveystacks.UserTeam;
import retrofit.surveystacks.UserTeamsAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Teams extends BaseActivity {

    SharedPrefManager sharedPrefManager;
    CardView cv1, cv2;
    TextView tv1, tv2, tv3;
    EditText et1;
    View v1, v2;
    RecyclerView rv1;
    ArrayList<UserTeam> teamsArrayList;
    Globals globals;
    Context context;
    UserTeamsAdapter userTeamsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame);
        getLayoutInflater().inflate(R.layout.teams, contentFrameLayout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_one_uui);
        navigationView.getMenu().getItem(7).setChecked(true);
        navigationView.getMenu().getItem(7).setEnabled(false);

        setTitle("Teams");

        sharedPrefManager = new SharedPrefManager(getApplicationContext());
        context = getApplicationContext();

        //CardView
        cv1 = findViewById(R.id.cv_one_teams);
        cv2 = findViewById(R.id.cv_two_teams);

        //TextView
        tv1 = findViewById(R.id.tv_one_teams);
        tv2 = findViewById(R.id.tv_two_teams);
        tv3 = findViewById(R.id.tv_three_teams);

        //EditText
        et1 = findViewById(R.id.et_one_teams);

        //View
        v1 = findViewById(R.id.v_one_teams);
        v2 = findViewById(R.id.v_two_teams);

        // Recycler view
        rv1 = findViewById(R.id.rv_one_teams);
        rv1.setHasFixedSize(true);
        rv1.setLayoutManager(new LinearLayoutManager(this));
        rv1.setItemViewCacheSize(20);
        rv1.setDrawingCacheEnabled(true);
        rv1.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        gatherTeamsData();
    }

    public void gatherTeamsData() {
        APIService apiService = RetroClient.getAPIService();

        Call<Session> call = apiService.teamsData(
                APIKeys.TEAMS_DATA[0],
                APIKeys.TEAMS_DATA[1],
                sharedPrefManager.getVK(),
                sharedPrefManager.getAK(),
                sharedPrefManager.getSK(),
                Locale.getDefault().getDisplayLanguage(),
                false
        );

        call.enqueue(new Callback<Session>() {
            @Override
            public void onResponse(Call<Session> call, Response<Session> response) {

                if(response.body().getResult_code() == 1) {
                    if (response.body().getUserTeams().size() != 0) {
                        rv1.setVisibility(View.VISIBLE);
                        teamsArrayList = response.body().getUserTeams();
                        userTeamsAdapter = new UserTeamsAdapter(context, teamsArrayList);
                        rv1.setAdapter(userTeamsAdapter);
                    } else {
                        tv3.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<Session> call, Throwable t) {

            }
        });
    }

    public void onJoinTeamClick(View view) {
    }
}