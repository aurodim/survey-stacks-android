package com.surveystacks.www.surveystacks;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.GsonBuilder;

import retrofit.surveystacks.APIService;
import retrofit.surveystacks.APIKeys;
import retrofit.surveystacks.RetroClient;
import retrofit.surveystacks.SessionInit;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecoveryCode extends AppCompatActivity {

    TextView tv1, tv2, tv3;
    EditText et1, et2, et3;
    Button btn1;
    ImageButton imb1;
    CardView cv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recovery_code);

        //TextViews
        tv1 = findViewById(R.id.tv_one_rc);
        tv2 = findViewById(R.id.tv_two_rc);
        tv3 = findViewById(R.id.tv_three_rc);
        //EditTexts
        et1 = findViewById(R.id.et_one_rc);
        et2 = findViewById(R.id.et_two_rc);
        et3 = findViewById(R.id.et_three_rc);
        //Buttons
        btn1 = findViewById(R.id.btn_one_rc);
        //ImageButtons
        imb1 = findViewById(R.id.imb_one_rc);
        //CardViews
        cv1 = findViewById(R.id.cv_one_rc);
        //Transitions
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    @Override
    public void onBackPressed() {
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
        super.onBackPressed();
    }

    public  void onRecoverCodeClick (View view) {
        //Progress Dialog
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("SurveyStacks");
        progressDialog.setMessage("Resetting password...");
        progressDialog.show();

        //String (EditText)
        String username = et1.getText().toString().trim();
        String recovery_code = et2.getText().toString().trim();
        String new_password = et3.getText().toString().trim();

        //Calling API controller
        APIService apiService = RetroClient.getAPIService();

        //Session Init
        Call<SessionInit> call = apiService.reset_password(
                APIKeys.RECOVERY_CODE,
                "en",
                username,
                recovery_code,
                new_password,
                false
        );

        //Request
        call.enqueue(new Callback<SessionInit>() {
            @Override
            public void onResponse(@NonNull Call<SessionInit> call, @NonNull Response<SessionInit> response) {
                progressDialog.dismiss();
                response.body();
                Log.i("r", new GsonBuilder().setPrettyPrinting().create().toJson(response.body()));
                switch (response.body().getResult_code()) {
                    case 1:
                        startActivity(new Intent(getApplicationContext(), Login.class));
                        Toast.makeText(RecoveryCode.this, "success", Toast.LENGTH_SHORT).show();
                        finish();
                        break;
                    case -1:
                        break;
                    case -2:
                        break;
                    case -3:
                        break;
                }
            }

            @Override
            public void onFailure(@NonNull Call<SessionInit> call, @NonNull Throwable t) {
                Toast.makeText(RecoveryCode.this, "Login Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
