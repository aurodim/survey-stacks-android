package com.surveystacks.www.surveystacks;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.GsonBuilder;

import java.util.Calendar;
import java.util.Locale;

import retrofit.surveystacks.APIService;
import retrofit.surveystacks.APIKeys;
import retrofit.surveystacks.RetroClient;
import retrofit.surveystacks.SessionInit;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPass extends AppCompatActivity {

    TextView tv1, tv2, tv3;
    EditText et1, et2, et3;
    Button btn1;
    ImageButton imb1;
    CardView cv1;

    Calendar myCalendar = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_pass);

        //TextViews
        tv1 = findViewById(R.id.tv_one_fp);
        tv2 = findViewById(R.id.tv_two_fp);
        tv3 = findViewById(R.id.tv_three_fp);
        //EditTexts
        et1 = findViewById(R.id.et_one_fp);
        et2 = findViewById(R.id.et_two_fp);
        et3 = findViewById(R.id.et_three_fp);
        //Buttons
        btn1 = findViewById(R.id.btn_one_fp);
        //ImageButtons
        imb1 = findViewById(R.id.imb_one_fp);
        //CardViews
        cv1 = findViewById(R.id.cv_one_fp);
        //Transitions
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
        //Calendar
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, month);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };
        //mm-dd-yyyy format
        et3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(ForgotPass.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    private void updateLabel() {
        String myFormat = "MM-dd-YYYY";
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
            et3.setText(sdf.format(myCalendar.getTime()));
            Log.i("date", sdf.format(myCalendar.getTime()));
        }
    }

    @Override
    public void onBackPressed() {
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
        super.onBackPressed();
    }

    public void onRecoverPasswordClick(View view) {
        //Progress Dialog
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("SurveyStacks");
        progressDialog.setMessage("Generating code...");
        progressDialog.show();

        //String (EditText)
        String username = et1.getText().toString().trim();
        String email_phoneNumber = et2.getText().toString().trim();
        String dob = et3.getText().toString().trim();

        //Calling API controller
        APIService apiService = RetroClient.getAPIService();

        //Session Init
        Call<SessionInit> call = apiService.forgot_password(
                APIKeys.FORGOT_PASSWORD,
                "en",
                username,
                email_phoneNumber,
                dob,
                false
        );

        //Request
        call.enqueue(new Callback<SessionInit>() {
            @Override
            public void onResponse(@NonNull Call<SessionInit> call, @NonNull Response<SessionInit> response) {
                progressDialog.dismiss();
                response.body();
                Log.i("r", new GsonBuilder().setPrettyPrinting().create().toJson(response.body()));
                switch (response.body().getResult_code()) {
                    case 1:
                        startActivity(new Intent(getApplicationContext(), RecoveryCode.class));
                        Toast.makeText(ForgotPass.this, "success", Toast.LENGTH_SHORT).show();
                        break;
                    case -1:
                        break;
                    case -2:
                        break;
                    case -3:
                        break;
                }
            }

            @Override
            public void onFailure(@NonNull Call<SessionInit> call, @NonNull Throwable t) {
                Toast.makeText(ForgotPass.this, "Login Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
