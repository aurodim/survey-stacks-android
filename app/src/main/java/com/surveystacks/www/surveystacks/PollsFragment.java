package com.surveystacks.www.surveystacks;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import retrofit.surveystacks.BookmarkAdapter;
import retrofit.surveystacks.Globals;
import retrofit.surveystacks.Poll;
import retrofit.surveystacks.SharedPrefManager;

public class PollsFragment extends Fragment {

    SharedPrefManager sharedPrefManager;
    Globals globals;
    Context context;
    ArrayList<Poll> pollArrayList;
    BookmarkAdapter pollAdapter;
    RecyclerView rv1;
    FragmentActivity listener;
    View view;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity){
            this.listener = (FragmentActivity) context;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.polls_fragment, parent, false);
        context = getActivity().getApplicationContext();
        sharedPrefManager = new SharedPrefManager(getActivity());
        globals = new Globals();
        pollArrayList = new ArrayList<>();
        rv1 = view.findViewById(R.id.rv_two_b);
        rv1.setHasFixedSize(true);
        rv1.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv1.setItemViewCacheSize(20);
        rv1.setDrawingCacheEnabled(true);
        rv1.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    public void bindData(ArrayList<Poll> data) {
        pollArrayList = data;
        pollAdapter = new BookmarkAdapter(getActivity(), null, pollArrayList, 1);
        pollAdapter.notifyDataSetChanged();
        rv1.setAdapter(pollAdapter);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.listener = null;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}