package com.surveystacks.www.surveystacks;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.GsonBuilder;

import java.util.Calendar;
import java.util.Locale;

import retrofit.surveystacks.APIService;
import retrofit.surveystacks.APIKeys;
import retrofit.surveystacks.CustomToast;
import retrofit.surveystacks.RetroClient;
import retrofit.surveystacks.SessionInit;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Register extends AppCompatActivity {

    TextView tv1, tv2, tv3;
    EditText et1, et2, et3, et4, et5;
    Button btn1;
    ImageButton imb1;
    RelativeLayout rl1;
    CardView cv1;

    Calendar myCalendar = Calendar.getInstance();
    Context context;
    CustomToast toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);

        //TextViews
        tv1 = findViewById(R.id.tv_one_register);
        tv2 = findViewById(R.id.tv_two_register);
        tv3 = findViewById(R.id.tv_three_register);
        //EditTexts
        et1 = findViewById(R.id.et_one_register);
        et2 = findViewById(R.id.et_two_register);
        et3 = findViewById(R.id.et_three_register);
        et4 = findViewById(R.id.et_four_register);
        et5 = findViewById(R.id.et_five_register);
        //Buttons
        btn1 = findViewById(R.id.btn_one_register);
        //ImageButtons
        imb1 = findViewById(R.id.imb_one_register);
        //RelativeLayout s
        rl1 = findViewById(R.id.rl_one_register);
        //CardViews
        cv1 = findViewById(R.id.cv_one_register);
        //Transitions
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
        //Calendar
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, month);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };
        //mm-dd-yyyy format
        et5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(Register.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    private void updateLabel() {
        String myFormat = "MM-dd-YYYY";
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
            et5.setText(sdf.format(myCalendar.getTime()));
            Log.i("date", sdf.format(myCalendar.getTime()));
        }
    }

    @Override
    public void onBackPressed() {
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
        super.onBackPressed();
    }

    public void onRegisterClick (View view) {
        //progress dialog
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("SurveyStacks");
        progressDialog.setMessage("Signing Up...");
        progressDialog.show();

        //String (EditText)
        String fullname = et1.getText().toString().trim();
        String email_phone = et2.getText().toString().trim();
        String username = et3.getText().toString().trim();
        String password = et4.getText().toString().trim();
        String dob = et5.getText().toString().trim();

        //Calling API controller
        APIService apiService = RetroClient.getAPIService();

        //Session Init
        Call<SessionInit> call = apiService.register(
                APIKeys.REGISTER,
                "en",
                fullname,
                email_phone,
                username,
                password,
                dob,
                false
        );

        //request
        call.enqueue(new Callback<SessionInit>() {
            @Override
            public void onResponse(@NonNull Call<SessionInit> call, @NonNull Response<SessionInit> response) {
                progressDialog.dismiss();
                Log.i("r", new GsonBuilder().setPrettyPrinting().create().toJson(response.body()));
                response.body();
                switch (response.body().getResult_code()) {
                    case 1:
//                        SharedPrefManager.getInstance(getApplicationContext())
//                                .storeSession(response.body().getProfile_auth_data("un"),
//                                        response.body().getProfile_auth_data("sk"),
//                                        response.body().getProfile_auth_data("lk"),
//                                        response.body().getProfile_auth_data("vk"),
//                                        response.body().getProfile_auth_data("sd"));
                        startActivity(new Intent(getApplicationContext(), BaseActivity.class));
                        Toast.makeText(Register.this, "success", Toast.LENGTH_SHORT).show();
                        break;
                    case -1:
                        break;
                    case -2:
                        break;
                    case -3:
                        break;
                }
            }

            @Override
            public void onFailure(Call<SessionInit> call, Throwable t) {
                Toast.makeText(Register.this, "Register failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onLoginActClick (View view) {
        Intent i = new Intent(this, Login.class);
        startActivity(i);
    }
}
