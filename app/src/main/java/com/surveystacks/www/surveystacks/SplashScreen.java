package com.surveystacks.www.surveystacks;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import retrofit.surveystacks.SharedPrefManager;

public class SplashScreen extends AppCompatActivity {

    SharedPrefManager sharedPrefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        sharedPrefManager = new SharedPrefManager(getApplicationContext());

        sharedPrefManager.setKeyInit(false);

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        if (SharedPrefManager.getInstance(getApplicationContext()).isLoggedIn()) {
                            startActivity(new Intent(getApplicationContext(), Surveys.class));
                        } else {
                            Intent i = new Intent(getApplicationContext(), Login.class);
                            startActivity(i);
                        }
                    }
                },
                1500);
    }
}
