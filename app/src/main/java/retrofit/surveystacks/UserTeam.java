package retrofit.surveystacks;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserTeam {
    public String getTeam_name() {
        return team_name;
    }

    public void setTeam_name(String team_name) {
        this.team_name = team_name;
    }

    public String getTeam_token() {
        return team_token;
    }

    public void setTeam_token(String team_token) {
        this.team_token = team_token;
    }

    public Boolean getExpanded() {
        return isExpanded;
    }

    public void setExpanded(Boolean expanded) {
        isExpanded = expanded;
    }

    @SerializedName("name")
    @Expose
    private String team_name;

    @SerializedName("token")
    @Expose
    private String team_token;

    @SerializedName("expand")
    @Expose
    private Boolean isExpanded;
}
