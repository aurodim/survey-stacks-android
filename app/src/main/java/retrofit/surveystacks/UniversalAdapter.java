package retrofit.surveystacks;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.surveystacks.www.surveystacks.R;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class UniversalAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private Context context;
    private LayoutInflater inflater;
    private List<Survey> surveys = Collections.emptyList();
    private List<Poll> polls = Collections.emptyList();
    private int Type;
    private Globals globals;

    public UniversalAdapter(Context context, List<Survey> surveys, List<Poll> polls, int type) {
        this.context = context;
        this.surveys = surveys;
        this.polls = polls;
        this.Type = type;
        inflater = LayoutInflater.from(context);
        globals = new Globals();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.bookmarked_holder_card, parent,false);
        return new UniversalAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final UniversalAdapter.Holder itemHolder = (UniversalAdapter.Holder) holder;
        String image_url;
        if (Type == 0) {
            Survey survey = surveys.get(position);
            image_url = globals.imageTransformation(survey.getImage_url(), 380, 380, "c_scale,f_auto,fl_lossy,q_auto", context, false);
        } else {
            Poll poll = polls.get(position);
            image_url = globals.imageTransformation(poll.getImage_url(), 380, 380, "c_scale,f_auto,fl_lossy,q_auto", context, false);
        }

        itemHolder.itemView.setVisibility(View.GONE);
        if (!Objects.equals(image_url, "__NA__")) {
            Glide.with(context).load(image_url).placeholder(R.color.colorWhite).override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL).listener(new RequestListener<String, GlideDrawable>() {
                @Override
                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                    itemHolder.itemView.setVisibility(View.VISIBLE);
                    return false;
                }

                @Override
                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                    itemHolder.itemView.setVisibility(View.VISIBLE);
                    return false;
                }
            }).into(itemHolder.imv1);
        }
    }

    @Override
    public int getItemCount() {
        return Type == 0 ? surveys.size() : polls.size();
    }

    class Holder extends RecyclerView.ViewHolder{

        ImageView imv1;

        Holder(View itemView) {
            super(itemView);
            imv1 = itemView.findViewById(R.id.imv_one_uhc);
        }

    }
}
