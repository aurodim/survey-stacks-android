package retrofit.surveystacks;

import android.util.Log;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class Session {

    @SerializedName("resultCode")
    @Expose
    private int result_code;

    @SerializedName("resultDate")
    @Expose
    private String result_date;

    public int getResult_code() {
        return result_code;
    }

    public void setResult_code(int result_code) {
        this.result_code = result_code;
    }

    public String getResult_date(){
        return result_date;
    }

    public void setResult_date(String result_date) {
        this.result_date = result_date;
    }

    //region Session Data
    @SerializedName("accountInfo")
    @Expose
    private AccountInfoSession accountInfoSession;

    @SerializedName("notificationsInfo")
    @Expose
    private NotificationInfoSession notificationInfoSession;

    public AccountInfoSession accountInfoSession() {
        return accountInfoSession;
    }

    public void setAccountInfoSession(AccountInfoSession accountInfoSession) {
        this.accountInfoSession = accountInfoSession;
    }

    public NotificationInfoSession notificationInfoSession() {
        return notificationInfoSession;
    }

    public void setNotificationInfoSession(NotificationInfoSession notificationInfoSession) {
        this.notificationInfoSession = notificationInfoSession;
    }

    public class AccountInfoSession {
        @SerializedName("profile_theme_url")
        @Expose
        private String profile_theme_url;

        @SerializedName("profile_picture_url")
        @Expose
        private String profile_picture_url;

        @SerializedName("stacks_amount")
        @Expose
        private int stacks;

        @SerializedName("coins_amount")
        @Expose
        private int coins;

        @SerializedName("payment_saved")
        @Expose
        private Boolean payment_saved;

        public String getProfile_theme_url() {
            return profile_theme_url;
        }

        public void setProfile_theme_url(String profile_theme_url) {
            this.profile_theme_url = profile_theme_url;
        }

        public String getProfile_picture_url() {
            return profile_picture_url;
        }

        public void setProfile_picture_url(String profile_picture_url) {
            this.profile_picture_url = profile_picture_url;
        }

        public int getStacks() {
            return stacks;
        }

        public void setStacks(int stacks) {
            this.stacks = stacks;
        }

        public int getCoins() {
            return coins;
        }

        public void setCoins(int coins) {
            this.coins = coins;
        }

        public Boolean getPayment_saved() {
            return payment_saved;
        }

        public void setPayment_saved(Boolean payment_saved) {
            this.payment_saved = payment_saved;
        }
    }

    private class NotificationInfoSession {
        @SerializedName("unread")
        @Expose
        private int unread;

        @SerializedName("list")
        @Expose
        private ArrayList list;

        public int getUnread() {
            return unread;
        }

        public void setUnread() {
            this.unread = unread;
        }

        public ArrayList getList() {
            return list;
        }

        public void setList(ArrayList list) {
            this.list = list;
        }
    }

    @SerializedName("csrf")
    @Expose
    private String csrf_token;

    public String getCsrf_token() {
        return csrf_token;
    }

    public void setCsrf_token(String csrf_token) {
        this.csrf_token = csrf_token;
    }
    //endregion

    //region Surveys Data
    @SerializedName("surveysData")
    @Expose
    private ArrayList<Survey> surveys = new ArrayList<>();

    public ArrayList<Survey> getSurveys(){
        return surveys;
    }

    public void setSurveys(ArrayList<Survey> surveys) {
        this.surveys = surveys;
    }
    //endregion

    //region Polls Data
    @SerializedName("pollsData")
    @Expose
    private ArrayList<Poll> polls = new ArrayList<>();

    public ArrayList<Poll> getPolls(){
        return polls;
    }

    public void setPolls(ArrayList<Poll> polls) {
        this.polls = polls;
    }
    //endregion

    //region Bookmarks

    @SerializedName("surveys_bookmarks")
    @Expose
    private ArrayList<Survey> surveysBookmarks = new ArrayList<>();

    @SerializedName("polls_bookmarks")
    @Expose
    private ArrayList<Poll> pollsBookmarks = new ArrayList<>();

    public ArrayList<Survey> getSurveysBookmarks(){
        return surveysBookmarks;
    }

    public void setSurveysBookmarks(ArrayList<Survey> surveysBookmarks) {
        this.surveysBookmarks = surveysBookmarks;
    }

    public ArrayList<Poll> getPollsBookmarks(){
        return pollsBookmarks;
    }

    public void setPollsBookmarks(ArrayList<Poll> pollsBookmarks) {
        this.pollsBookmarks = pollsBookmarks;
    }
    //endregion

    //region Teams
    @SerializedName("teams")
    @Expose
    private ArrayList<UserTeam> userTeams = new ArrayList<>();

    public ArrayList<UserTeam> getUserTeams() {
        return userTeams;
    }

    public void setUserTeams(ArrayList<UserTeam> userTeams) {
        this.userTeams = userTeams;
    }
    //endregion

    //region Manager
    @SerializedName("surveys")
    @Expose
    private ArrayList<Survey> surveysManager = new ArrayList<>();

    @SerializedName("polls")
    @Expose
    private ArrayList<Poll> pollsManager = new ArrayList<>();

    public ArrayList<Survey> getSurveysManager() {
        return surveysManager;
    }

    public void setSurveysManager(ArrayList<Survey> surveysManager) {
        this.surveysManager = surveysManager;
    }

    public ArrayList<Poll> getPollsManager() {
        return pollsManager;
    }

    public void setPollsManager(ArrayList<Poll> pollsManager) {
        this.pollsManager = pollsManager;
    }
    //endregion

    //region Categories
    @SerializedName("categories")
    @Expose
    private Map<String, String> categories = new HashMap<String, String>();


    public Map<String, String> getCategories() {
        return categories;
    }

    public void setCategories(Map<String, String> categories) {
        this.categories = categories;
    }
    //endregion
}
