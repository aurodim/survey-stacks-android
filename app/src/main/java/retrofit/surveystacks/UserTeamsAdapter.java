package retrofit.surveystacks;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.surveystacks.www.surveystacks.R;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public  class UserTeamsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    private List<UserTeam> userTeams = Collections.emptyList();
    private Globals globals;
    private String token;
    private Boolean isExpanded;

    public UserTeamsAdapter(Context context, List<UserTeam> userTeams) {
        this.context = context;
        this.userTeams = userTeams;
        inflater = LayoutInflater.from(context);
        globals = new Globals();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= inflater.inflate(R.layout.team_cardholder, parent,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final Holder teamHolder = (Holder) holder;
        UserTeam userTeam = userTeams.get(position);

        teamHolder.tv1.setText(userTeam.getTeam_name());
        token = userTeam.getTeam_token();
        isExpanded = userTeam.getExpanded();
    }

    @Override
    public int getItemCount() {
        return userTeams.size();
    }

    class Holder extends RecyclerView.ViewHolder{

        CardView cv1;
        TextView tv1;
        Button btn1;

        Holder(View itemView) {
            super(itemView);
            cv1 = itemView.findViewById(R.id.cv_one_teams_h);
            tv1 = itemView.findViewById(R.id.tv_one_teams_h);
            btn1 = itemView.findViewById(R.id.btn_one_teams_h);
            cv1.setOnClickListener(expandCardListener);
        }

        private View.OnClickListener expandCardListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isExpanded) {
                    btn1.setVisibility(View.GONE);
                    isExpanded = false;
                } else {
                    btn1.setVisibility(View.VISIBLE);
                    isExpanded = true;
                }
            }
        };
    }
}
