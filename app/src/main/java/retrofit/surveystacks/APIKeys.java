package retrofit.surveystacks;

/**
 * Created by isaac on 1/22/18.
 */

public class APIKeys {
    // POST
    public static String LOGIN = "86d6b440f80df257d08375ac9719c24a7828e075";
    public static String REGISTER = "ff6e14a65abe095a37bd7e203da37183553a0ae4";
    public static String FORGOT_USERNAME = "b7a1992ab58f4923369f3a53f0b73d02f71368f1";
    public static String FORGOT_PASSWORD = "106b965c50810171f7ad65198ebeb5c1aa153e7f";
    public static String RECOVERY_CODE = "1598e8e31a66516d8fd27f15a6190904a31382c9";
    public static String ABOUT_SURVEY = "f877990c0256d27d0ab9c28f05eaf6d3b7748502";
    public static String PAYMENT_SURVEY = "dd1fe2a3422d0b1e8b767377e428d12c9098b917";
    public static String DELETE_ACCOUNT = "fb11b53abcc1b8f696ae7103337e466b5cc28f2a";
    public static String LABS_CREATE = "1ff46b49c380c942733b4d89ac2c86f9602affa5";
    public static String CREATE_SURVEY = "5d83912d2037f1c843caa61125c59a3b2be4f7d0";
    public static String CREATE_POLL = "0c60247b9e8d9e2d67bf8f1ea07894c63af6ddca";
    public static String ANSWER_POLL = "f6642b19507252dbd126a770e9ea0265c3754bd5";
    public static String ANSWER_SURVEY = "7ccea46ffe440f91a95c84272c919fbe9057c80e";
    public static String WISHLIST = "6da2c39b7d41fd5bb5d545173db09988df3e9364";
    public static String REPORT = "d1c8c9bd26d6083413e43b80948525ce05cab0ba";
    public static String EDITOR = "b922425ee2940e6af1564f105a6911d1c757a3a6";
    public static String JOIN_TEAM = "e956030f6144dc88e583646a24cdfba30d4aebd2";
    public static String LEAVE_TEAM = "5faaa9f469a85c282b6ec117cc3a8bae32a4fcec";
    public static String BOOKMARK = "c0905eeacea2815d5a7877f4fe35a38c94c81e46";
    public static String COINSTSTACKS = "1fb87bc55c7531db41295a99d7b4f3e5";
    public static String REDEEM = "318f00d5aaa397e312d4e88773128b25";
    public static String UPDATE_PFP = "29cf0a5f4e05b9cbcb7e3b08da0109d576b8d33f";
    public static String UPDATE_MOTTO = "n0102esa2el2l1ed9a78a7m4fe35a38c94c81e46";
    public static String ACCOUNT_SETTINGS = "684f09b83c13cfc2249d637db86e0bea0e007af1";
    public static String CONTACT_SETTINGS = "7da38a622ab17c090389d806efa1c785a2243977";
    public static String LABS_SETTINGS = "39b95a7aea064635f4f6fec22d45bddfe05be85d";
    public static String PASSWORD_SETTINGS = "a6e86db25db01eb61f9c8b4950b3914ee2f33e87";
    public static String NOTIFICATION_SETTINGS = "d4b015c242d095eb62b0acabfac0a8f88d7c9cef";
    public static String PREFERENCES_SURVEY = "6de84acd7ebbc319dbd884ce6ddb816172d9b5fc";

    // GET
    public static String SESSION_DATA[] = {"WednesdayNightClearSky", "990178"};
    public static String SURVEYS_DATA[] = {"MondayEveningBeautifulSky", "991212"};
    public static String ABOUT_DATA[] = {"MondayNightDark", "999666"};
    public static String PAYMENT_DATA[] = {"TuesdayAfternoonQuarterToEvening", "929647"};
    public static String PROFILE_DATA[] = {"ThursdayNightBrightMoon", "991756"};
    public static String PUBLIC_PROFILE_DATA[] = {"FridayEveningCloudySky", "995215"};
    public static String REWARDS_DATA[] = {"FridayEvening1124", "991124"};
    public static String MANAGE_DATA[] = {"WednesdayNightThanksgivingEve", "954262"};
    public static String BOOKMARKS_DATA[] = {"SaturdayAfternoonCloudy", "021299"};
    public static String LABS_DATA[] = {"SundaySnowingSlow", "990211"};
    public static String EDITOR_DATA[] = {"Thanksgiving3am", "991123"};
    public static String RECEIPT_DATA[] = {"Thanksgiving1pm", "991213"};
    public static String SETTINGS_DATA[] = {"TuesdayEveningSunnyClouds", "991223"};
    public static String POLLS_DATA[] = {"SundayEveningBTS", "991126"};
    public static String ANALYTICS_DATA[] = {"TuesdayEveningWindy", "991205"};
    public static String TEAMS_DATA[] = {"SaturdayEveningLate", "991318"};
    public static String REPORT_DATA[] = {"SaturdayMidnight2", "991202"};
    public static String PREFERENCES_DATA[] = {"FridayNightLunaCat", "990675"};
    public static String POLL_ANSWER_DATA[] = {"SundayNightAC", "990220"};
    public static String SURVEY_ANSWER_DATA[] = {"FridayNightMMS", "991201"};
    public static String SEARCH_DATA[] = {"WednesdayNightEmptySky", "990128"};
    public static String CREATOR_DATA[] = {"ThursdayEveningLightBlueSky", "990304"};
    public static String CATEGORIES[] = {"FridayMorningPreMeet", "993421"};
    public static String NOTIFICATIONS_DATA[] = {"TuesdayEveningPizza", "992018"};
    public static String X[] = {"", ""};
    public static String Y[] = {"", ""};
    public static String Z[] = {"", ""};
}
