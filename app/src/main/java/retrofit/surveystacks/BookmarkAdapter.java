package retrofit.surveystacks;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.surveystacks.www.surveystacks.R;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class BookmarkAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    private List<Survey> surveys = Collections.emptyList();
    private List<Poll> polls = Collections.emptyList();
    private int Type;
    private Globals globals;
    private CustomToast customToast;

    public BookmarkAdapter(Context context, List<Survey> surveys, List<Poll> polls, int type) {
        this.context = context;
        this.surveys = surveys;
        this.polls = polls;
        this.Type = type;
        inflater = LayoutInflater.from(context);
        globals = new Globals();
        customToast = new CustomToast();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.bookmarked_holder_card, parent,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        final Holder itemHolder = (Holder) holder;
        String image_url;
        if (Type == 0) {
            Survey survey = surveys.get(position);
            image_url = globals.imageTransformation(survey.getImage_url(), 380, 380, "c_scale,f_auto,fl_lossy,q_auto", context, false);
        } else {
            Poll poll = polls.get(position);
            image_url = globals.imageTransformation(poll.getImage_url(), 380, 380, "c_scale,f_auto,fl_lossy,q_auto", context, false);
        }

        itemHolder.itemView.setVisibility(View.GONE);
        if (!Objects.equals(image_url, "__NA__")) {
            Glide.with(context).load(image_url).placeholder(R.color.colorWhite).override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL).listener(new RequestListener<String, GlideDrawable>() {
                @Override
                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                    itemHolder.itemView.setVisibility(View.VISIBLE);
                    return false;
                }

                @Override
                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                    itemHolder.itemView.setVisibility(View.VISIBLE);
                    return false;
                }
            }).into(itemHolder.imv1);
        }

        itemHolder.imv1.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                itemHolder.v1.setVisibility(View.VISIBLE);
                itemHolder.imv2.setVisibility(View.VISIBLE);

                itemHolder.v1.setScaleY(0.1f);
                itemHolder.v1.setScaleX(0.1f);
                itemHolder.v1.setAlpha(1f);
                itemHolder.imv2.setScaleY(0.1f);
                itemHolder.imv2.setScaleX(0.1f);

                AnimatorSet animatorSet = new AnimatorSet();

                ObjectAnimator bgScaleYAnim = ObjectAnimator.ofFloat(itemHolder.v1, "scaleY", 0.1f, 1f);
                bgScaleYAnim.setDuration(200);
                bgScaleYAnim.setInterpolator(new DecelerateInterpolator());
                ObjectAnimator bgScaleXAnim = ObjectAnimator.ofFloat(itemHolder.v1, "scaleX", 0.1f, 1f);
                bgScaleXAnim.setDuration(200);
                bgScaleXAnim.setInterpolator(new DecelerateInterpolator());
                ObjectAnimator bgAlphaAnim = ObjectAnimator.ofFloat(itemHolder.v1, "alpha", 1f, 0f);
                bgAlphaAnim.setDuration(200);
                bgAlphaAnim.setStartDelay(150);
                bgAlphaAnim.setInterpolator(new DecelerateInterpolator());

                ObjectAnimator imgScaleUpYAnim = ObjectAnimator.ofFloat(itemHolder.imv2, "scaleY", 0.1f, 2f);
                imgScaleUpYAnim.setDuration(300);
                imgScaleUpYAnim.setInterpolator(new DecelerateInterpolator());
                ObjectAnimator imgScaleUpXAnim = ObjectAnimator.ofFloat(itemHolder.imv2, "scaleX", 0.1f, 2f);
                imgScaleUpXAnim.setDuration(300);
                imgScaleUpXAnim.setInterpolator(new DecelerateInterpolator());

                ObjectAnimator imgScaleDownYAnim = ObjectAnimator.ofFloat(itemHolder.imv2, "scaleY", 2f, 0f);
                imgScaleDownYAnim.setDuration(300);
                imgScaleDownYAnim.setInterpolator(new AccelerateInterpolator());
                ObjectAnimator imgScaleDownXAnim = ObjectAnimator.ofFloat(itemHolder.imv2, "scaleX", 2f, 0f);
                imgScaleDownXAnim.setDuration(300);
                imgScaleDownXAnim.setInterpolator(new AccelerateInterpolator());

                animatorSet.playTogether(bgScaleYAnim, bgScaleXAnim, bgAlphaAnim, imgScaleUpYAnim, imgScaleUpXAnim);
                animatorSet.play(imgScaleDownYAnim).with(imgScaleDownXAnim).after(imgScaleUpYAnim);

                animatorSet.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        itemHolder.itemView.setVisibility(View.GONE);
                        if (Type == 0) {
                            surveys.remove(position);
                        } else {
                            polls.remove(position);
                        }
                        notifyDataSetChanged();
                    }
                });
                animatorSet.start();
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return Type == 0 ? surveys.size() : polls.size();
    }

    class Holder extends RecyclerView.ViewHolder{

        ImageView imv1;
        ImageView imv2;
        View v1;

        Holder(View itemView) {
            super(itemView);
            imv1 = itemView.findViewById(R.id.imv_one_bhc);
            imv2 = itemView.findViewById(R.id.imv_two_bhc);
            v1 = itemView.findViewById(R.id.v_one_bhc);
        }
    }
}
