package retrofit.surveystacks;

import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.surveystacks.www.surveystacks.R;

public class CustomToast {

    public void showCustomToast(Context context, String response, int length) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View TSL = layoutInflater.inflate(R.layout.custom_toast, (ViewGroup) ((Activity) context).findViewById(R.id.rl_one_ct));
        TextView toastText = TSL.findViewById(R.id.tv_one_ct);
        android.widget.Toast custom_toast = new android.widget.Toast(context.getApplicationContext());
        toastText.setText(response);
        custom_toast.setGravity(Gravity.CENTER | Gravity.BOTTOM, 0, 0);
        custom_toast.setDuration(length);
        custom_toast.setView(TSL);
        custom_toast.show();
    }
}
