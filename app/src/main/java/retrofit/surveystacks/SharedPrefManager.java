package retrofit.surveystacks;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.Objects;

/**
 * Created by SSDevTeam on 1/22/18.
 */

public class SharedPrefManager {

    @SuppressLint("StaticFieldLeak")
    private static SharedPrefManager mInstance;
    @SuppressLint("StaticFieldLeak")
    private static Context mCtx;

    private static final String SHARED_PREF_NAME = "_ss_storage";

    private static final String KEY_UN = "un";
    private static final String KEY_SK = "sk";
    private static final String KEY_LK = "lk";
    private static final String KEY_VK = "vk";
    private static final String KEY_SC = "sd";
    private static final String KEY_CSRF = "csrf";
    private static final String KEY_PFP = "pfp";
    private static final String KEY_PTP = "ptp";
    private static final String KEY_Stacks = "stacks";
    private static final String KEY_Coins = "coins";
    private static final String KEY_INIT = "__session__innit__";

    public SharedPrefManager(Context context) {
        mCtx = context;
    }

    public static synchronized SharedPrefManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPrefManager(context);
        }
        return mInstance;
    }

    public void storeSession(String un, String sk, String lk, String vk, String sd) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_UN, un);
        editor.putString(KEY_SK, sk);
        editor.putString(KEY_LK, lk);
        editor.putString(KEY_VK, vk);
        editor.putBoolean(KEY_SC, Boolean.parseBoolean(sd));
        editor.apply();
    }

    public boolean isLoggedIn() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return !Objects.equals(sharedPreferences.getString(KEY_UN, ""), "") && !Objects.equals(sharedPreferences.getString(KEY_LK, ""), "") && !Objects.equals(sharedPreferences.getString(KEY_VK, ""), "") &&
                !Objects.equals(sharedPreferences.getString(KEY_SK, ""), "") && !Objects.equals(sharedPreferences.getString(KEY_CSRF, ""), "");
    }

    public boolean logout() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
        return true;
    }

    public String getVK() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_VK, "");
    }

    public String getSK() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_SK, "");
    }

    public String getAK() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_LK, "");
    }

    public String getCSRF() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_CSRF, "");
    }

    public String getKeyPfp() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_PFP, "");
    }

    public String getKeyPtp() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_PTP, "");
    }

    public int getKEY_Stacks() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(KEY_Stacks, -1);
    }

    public int getKEY_Coins() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(KEY_Coins, -1);
    }

    public String getUsername() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_UN, "");
    }

    public void setCSRF(String token) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_CSRF, token);
        editor.apply();
    }

    public void setKeyPfp(String url) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_PFP, url);
        editor.apply();
    }

    public void setKeyPtp(String url) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_PTP, url);
        editor.apply();
    }

    public void setKEY_Stacks(int stacks) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(KEY_Stacks, stacks);
        editor.apply();
    }

    public void setKEY_Coins(int coins) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(KEY_Coins, coins);
        editor.apply();
    }

    public Boolean getSessionInit() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(KEY_INIT, false);
    }

    public void setKeyInit(Boolean init) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(KEY_INIT, init);
        editor.apply();
    }
}
