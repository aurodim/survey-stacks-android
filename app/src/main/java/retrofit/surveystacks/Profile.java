package retrofit.surveystacks;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.surveystacks.www.surveystacks.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Profile {

    @SerializedName("resultCode")
    @Expose
    private int result_code;

    @SerializedName("resultDate")
    @Expose
    private String result_date;

    public int getResult_code() {
        return result_code;
    }

    public void setResult_code(int result_code) {
        this.result_code = result_code;
    }

    public String getResult_date(){
        return result_date;
    }

    public void setResult_date(String result_date) {
        this.result_date = result_date;
    }

    @SerializedName("accountInfo")
    @Expose
    private AccountInfoProfile accountInfoProfile;

    @SerializedName("achievementsInfo")
    @Expose
    private AchievementsInfoProfile achievementsInfoProfile;

    @SerializedName("contactInfo")
    @Expose
    private ContactInfoProfile contactInfoProfile;

    public AccountInfoProfile accountInfoProfile() {
        return accountInfoProfile;
    }

    public void setAccountInfoProfile(AccountInfoProfile accountInfoProfile) {
        this.accountInfoProfile = accountInfoProfile;
    }

    public AchievementsInfoProfile achievementsInfoProfile() {
        return achievementsInfoProfile;
    }

    public void setAchievementsInfoProfile(AchievementsInfoProfile achievementsInfoProfile) {
        this.achievementsInfoProfile = achievementsInfoProfile;
    }

    public ContactInfoProfile contactInfoProfile() {
        return contactInfoProfile;
    }

    public void setContactInfoProfile(ContactInfoProfile contactInfoProfile) {
        this.contactInfoProfile = contactInfoProfile;
    }


    public class AccountInfoProfile {
        @SerializedName("profile_theme_url")
        @Expose
        private String profile_theme_url;

        @SerializedName("profile_picture_url")
        @Expose
        private String profile_picture_url;

        @SerializedName("motto")
        @Expose
        private String motto;

        @SerializedName("since")
        @Expose
        private long member_since;

        public String getProfile_theme_url() {
            return profile_theme_url;
        }

        public void setProfile_theme_url(String profile_theme_url) {
            this.profile_theme_url = profile_theme_url;
        }

        public String getProfile_picture_url() {
            return profile_picture_url;
        }

        public void setProfile_picture_url(String profile_picture_url) {
            this.profile_picture_url = profile_picture_url;
        }

        public String getMotto() {
            return motto;
        }

        public void setMotto(String motto) {
            this.motto = motto;
        }

        public String getMember_since(Context context) {
            SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy", context.getResources().getConfiguration().locale);
            Date since = new Date(member_since);
            return String.format(context.getResources().getString(R.string.member_since), format.format(since));
        }

        public void setMember_since(int member_since) {
            this.member_since = member_since;
        }
    }

    public class AchievementsInfoProfile {

        @SerializedName("stacks_earned")
        @Expose
        private int lifetime_stacks;

        @SerializedName("coins_earned")
        @Expose
        private int lifetime_coins;

        @SerializedName("questions_answered")
        @Expose
        private int questions_answered;

        @SerializedName("surveys_answered")
        @Expose
        private int surveys_answered;

        @SerializedName("rewards_claimed")
        @Expose
        private int rewards_claimed;

        public String getLifetime_stacks(Context context) {
            return String.format(context.getResources().getString(R.string.lifetime_stacks), lifetime_stacks);
        }

        public void setLifetime_stacks(int lifetime_stacks) {
            this.lifetime_stacks = lifetime_stacks;
        }

        public String getLifetime_coins(Context context) {
            return String.format(context.getResources().getString(R.string.lifetime_coins), lifetime_coins);
        }

        public void setLifetime_coins(int lifetime_coins) {
            this.lifetime_coins = lifetime_coins;
        }

        public int getQuestions_answered() {
            return questions_answered;
        }

        public void setQuestions_answered(int questions_answered) {
            this.questions_answered = questions_answered;
        }

        public int getSurveys_answered() {
            return surveys_answered;
        }

        public void setSurveys_answered(int surveys_answered) {
            this.surveys_answered = surveys_answered;
        }

        public int getRewards_claimed() {
            return rewards_claimed;
        }

        public void setRewards_claimed(int rewards_claimed) {
            this.rewards_claimed = rewards_claimed;
        }
    }

    public class ContactInfoProfile {
        @SerializedName("email")
        @Expose
        private String email;

        @SerializedName("phone_number")
        @Expose
        private String phone_number;

        @SerializedName("website")
        @Expose
        private String website;

        @SerializedName("facebook")
        @Expose
        private String facebook;

        @SerializedName("instagram")
        @Expose
        private String instagram;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone_number() {
            return phone_number;
        }

        public void setPhone_number(String phone_number) {
            this.phone_number = phone_number;
        }

        public String getWebsite() {
            return website;
        }

        public void setWebsite(String website) {
            this.website = website;
        }

        public String getFacebook() {
            return facebook;
        }

        public void setFacebook(String facebook) {
            this.facebook = facebook;
        }

        public String getInstagram() {
            return instagram;
        }

        public void setInstagram(String instagram) {
            this.instagram = instagram;
        }
    }
}
