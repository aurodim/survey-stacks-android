package retrofit.surveystacks;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.surveystacks.www.surveystacks.R;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public  class PollAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    private List<Poll> polls = Collections.emptyList();
    private Globals globals;

    public PollAdapter(Context context, List<Poll> polls) {
        this.context = context;
        this.polls = polls;
        inflater = LayoutInflater.from(context);
        globals = new Globals();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= inflater.inflate(R.layout.poll_holder_card, parent,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final Holder pollHolder = (Holder) holder;
        Poll item = polls.get(position);

        String image_url = globals.imageTransformation(item.getImage_url(), 400, 350, "c_scale,f_auto,fl_lossy,q_auto", context, false);
        String c_image_url = globals.imageTransformation(item.getCreator_image_url(), 75, 75, "c_fill,r_50,f_auto,fl_lossy,q_auto", context, true);
        pollHolder.tv1.setText(item.getTitle());
        pollHolder.tv2.setText(item.getDescription());
        pollHolder.tv3.setText(item.getCoins());
        pollHolder.tv4.setText(item.getCreator());
        pollHolder.itemView.setVisibility(View.GONE);
        if (!Objects.equals(image_url, "__NA__")) {
            Glide.with(context).load(image_url).placeholder(R.color.colorWhite).override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL).listener(new RequestListener<String, GlideDrawable>() {
                @Override
                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                    pollHolder.itemView.setVisibility(View.VISIBLE);
                    return false;
                }

                @Override
                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                    pollHolder.itemView.setVisibility(View.VISIBLE);
                    return false;
                }
            }).into(pollHolder.imb1);
        }
        if (!Objects.equals(c_image_url, "__NA__")) {
            Glide.with(context).load(c_image_url).override(globals.dpToPx(75, context), globals.dpToPx(75, context)).into(pollHolder.imb2);
        }
    }

    @Override
    public int getItemCount() {
        return polls.size();
    }

    class Holder extends RecyclerView.ViewHolder{

        ImageView imb1, imb2;
        TextView tv1, tv2, tv3, tv4;

        Holder(View itemView) {
            super(itemView);
            imb1 = itemView.findViewById(R.id.imb_one_ph);
            tv1 = itemView.findViewById(R.id.tv_one_ph);
            tv2 = itemView.findViewById(R.id.tv_two_ph);
            tv3 = itemView.findViewById(R.id.tv_three_ph);
            imb2 = itemView.findViewById(R.id.imb_two_ph);
            tv4 = itemView.findViewById(R.id.tv_four_ph);
        }

    }
}
