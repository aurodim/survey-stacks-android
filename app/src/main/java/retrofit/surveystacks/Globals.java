package retrofit.surveystacks;


import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;

import java.util.Objects;

public class Globals {

    int pxToDp(int px, Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return (int) Math.ceil(px / (displayMetrics.density));
    }

    int dpToPx(int dp, Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return (int) Math.ceil(dp * (displayMetrics.density));
    }

    public String imageTransformation(String url, int width, int height, String type, Context context, Boolean dp) {
        if (Objects.equals(url, "")) return "__NA__";
        String parts[] = url.split("upload/");
        parts[0] = parts[0] + "upload/";

        String noTrans[] = parts[1].split("/v");
        if (noTrans.length != 1) {
            parts[1] = "/v" + noTrans[1];
        } else {
            parts[1] = "/" + parts[1];
        }

        if (dp) {
            width = dpToPx(width, context);
            height = dpToPx(height, context);
        }

        String transformation = "";

        if (width != 0 && height != 0 && !Objects.equals(type, "")) {
            transformation = String.format("%s,w_%s,h_%s", type, width, height);
        } else if (width != 0 && height != 0) {
            transformation = String.format("w_%s,h_%s", width, height);
        } else if (width != 0 && !Objects.equals(type, "")) {
            transformation = String.format("%s,w_%s", type, width);
        } else if (width != 0) {
            transformation = String.format("w_%s", width);
        } else if (height != 0 && !Objects.equals(type, "")) {
            transformation = String.format("%s,h_%s", type, height);
        } else if (height != 0) {
            transformation = String.format("h_%s", width);
        }

        return parts[0] + transformation + parts[1];
    }
}
