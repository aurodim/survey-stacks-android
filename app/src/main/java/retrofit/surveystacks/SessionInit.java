package retrofit.surveystacks;

import android.util.Log;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by SSDevTeam on 1/22/18.
 */

public class SessionInit {

    @SerializedName("resultCode")
    @Expose
    private int result_code;

    @SerializedName("resultDate")
    @Expose
    private String result_date;

    @SerializedName("profile_auth_data")
    @Expose
    private ProfileAuthData profileAuthData;

    public int getResult_code() {
        return result_code;
    }

    public void setResult_code(int result_code) {
        this.result_code = result_code;
    }

    public String getResult_date() {
        return result_date;
    }

    public void setResult_date(String result_date) {
        this.result_date = result_date;
    }

    public ProfileAuthData getProfileAuthData() {
        return profileAuthData;
    }

    public void setProfileAuthData(ProfileAuthData profileAuthData) {
        this.profileAuthData = profileAuthData;
    }

    public class ProfileAuthData {
        @SerializedName("un")
        @Expose
        private String username;

        @SerializedName("vk")
        @Expose
        private String validation_key;

        @SerializedName("sk")
        @Expose
        private String session_key;

        @SerializedName("lk")
        @Expose
        private String auth_key;

        @SerializedName("sd")
        @Expose
        private Boolean setup_completed;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getValidation_key() {
            return validation_key;
        }

        public void setValidation_key(String validation_key) {
            this.validation_key = validation_key;
        }

        public String getSession_key() {
            return session_key;
        }

        public void setSession_key(String session_key) {
            this.session_key = session_key;
        }

        public String getAuth_key() {
            return auth_key;
        }

        public void setAuth_key(String auth_key) {
            this.auth_key = auth_key;
        }

        public String getSetup_completed() {
            return String.valueOf(setup_completed);
        }

        public void setSetup_completed(Boolean setup_completed) {
            this.setup_completed = setup_completed;
        }
    }
}
