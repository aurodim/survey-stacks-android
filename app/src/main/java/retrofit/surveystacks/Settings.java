package retrofit.surveystacks;


import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.surveystacks.www.surveystacks.R;

import java.util.ArrayList;

public class Settings {
    @SerializedName("resultCode")
    @Expose
    private int result_code;

    @SerializedName("resultDate")
    @Expose
    private String result_date;

    public int getResult_code() {
        return result_code;
    }

    public void setResult_code(int result_code) {
        this.result_code = result_code;
    }

    public String getResult_date(){
        return result_date;
    }

    public void setResult_date(String result_date) {
        this.result_date = result_date;
    }


    public AccountInfoSettings getAccountInfoSettings() {
        return accountInfoSettings;
    }

    public void setAccountInfoSettings(AccountInfoSettings accountInfoSettings) {
        this.accountInfoSettings = accountInfoSettings;
    }

    public ContactInfoSettings getContactInfoSettings() {
        return contactInfoSettings;
    }

    public void setContactInfoSettings(ContactInfoSettings contactInfoSettings) {
        this.contactInfoSettings = contactInfoSettings;
    }

    public NotificationsInfoSettings getNotificationsInfo() {
        return notificationsInfo;
    }

    public void setNotificationsInfo(NotificationsInfoSettings notificationsInfo) {
        this.notificationsInfo = notificationsInfo;
    }

    public AboutInfoSettings getAboutInfoSettings() {
        return aboutInfoSettings;
    }

    public void setAboutInfoSettings(AboutInfoSettings aboutInfoSettings) {
        this.aboutInfoSettings = aboutInfoSettings;
    }

    public PreferencesInfoSettings getPreferencesInfoSettings() {
        return preferencesInfoSettings;
    }

    public void setPreferencesInfoSettings(PreferencesInfoSettings preferencesInfoSettings) {
        this.preferencesInfoSettings = preferencesInfoSettings;
    }

    public PaymentInfoSettings getPaymentInfoSettings() {
        return paymentInfoSettings;
    }

    public void setPaymentInfoSettings(PaymentInfoSettings paymentInfoSettings) {
        this.paymentInfoSettings = paymentInfoSettings;
    }

    @SerializedName("accountInfo")
    @Expose
    private AccountInfoSettings accountInfoSettings;

    @SerializedName("contactInfo")
    @Expose
    private ContactInfoSettings contactInfoSettings;

    @SerializedName("notificationsInfo")
    @Expose
    private NotificationsInfoSettings notificationsInfo;

    @SerializedName("aboutInfo")
    @Expose
    private AboutInfoSettings aboutInfoSettings;

    @SerializedName("preferencesInfo")
    @Expose
    private PreferencesInfoSettings preferencesInfoSettings;

    @SerializedName("paymentInfo")
    @Expose
    private PaymentInfoSettings paymentInfoSettings;

    public class AccountInfoSettings {
        public String getEmailPhone() {
            return email_phone;
        }

        public void setEmailPhone(String email_phone) {
            this.email_phone = email_phone;
        }

        @SerializedName("ep")
        @Expose
        private String email_phone;
    }

    public class ContactInfoSettings {
        @SerializedName("email")
        @Expose
        private String email;

        @SerializedName("phone_number")
        @Expose
        private String phone_number;

        @SerializedName("website")
        @Expose
        private String website;

        @SerializedName("facebook")
        @Expose
        private String facebook;

        @SerializedName("instagram")
        @Expose
        private String instagram;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone_number() {
            return phone_number;
        }

        public void setPhone_number(String phone_number) {
            this.phone_number = phone_number;
        }

        public String getWebsite() {
            return website;
        }

        public void setWebsite(String website) {
            this.website = website;
        }

        public String getFacebook() {
            return facebook;
        }

        public void setFacebook(String facebook) {
            this.facebook = facebook;
        }

        public String getInstagram() {
            return instagram;
        }

        public void setInstagram(String instagram) {
            this.instagram = instagram;
        }
    }

    public class NotificationsInfoSettings {
        public Boolean getEmail_phone_notif() {
            return email_phone_notif;
        }

        public void setEmail_phone_notif(Boolean email_phone_notif) {
            this.email_phone_notif = email_phone_notif;
        }

        public Boolean getMatch_pref_notif() {
            return match_pref_notif;
        }

        public void setMatch_pref_notif(Boolean match_pref_notif) {
            this.match_pref_notif = match_pref_notif;
        }

        @SerializedName("email_message_notifications")
        @Expose
        private Boolean email_phone_notif;

        @SerializedName("match_preferences_notifications")
        @Expose
        private Boolean match_pref_notif;
    }

    public class AboutInfoSettings {
        public Boolean getBlank() {
            return isBlank;
        }

        public void setBlank(Boolean blank) {
            isBlank = blank;
        }

        public String getSex(Context applicationContext) {
            return String.format(applicationContext.getResources().getString(R.string.sex_parse), sex);
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getEthnicity(Context applicationContext) {
            StringBuilder output = new StringBuilder();
            for (int i = 0; i < ethnicity.size(); i++) {
                if (i+1 == ethnicity.size()) {
                    output.append(ethnicity.get(i)).append("");
                    continue;
                }
                output.append(ethnicity.get(i)).append(", ");
            }
            return String.format(applicationContext.getResources().getString(R.string.ethnicity_parse),output.toString());
        }

        public void setEthnicity(ArrayList<String> ethnicity) {
            this.ethnicity = ethnicity;
        }

        public String getLang(Context applicationContext) {
            StringBuilder output = new StringBuilder();
            for (int i = 0; i < lang.size(); i++) {
                if (i+1 == lang.size()) {
                    output.append(lang.get(i)).append("");
                    continue;
                }
                output.append(lang.get(i)).append(", ");
            }
            return String.format(applicationContext.getResources().getString(R.string.languages_spoken_parse), output.toString());
        }

        public void setLang(ArrayList<String> lang) {
            this.lang = lang;
        }

        public String getEducation(Context applicationContext) {
            return String.format(applicationContext.getResources().getString(R.string.education_parse), education);
        }

        public void setEducation(String education) {
            this.education = education;
        }

        public String getRelationship(Context applicationContext) {
            return String.format(applicationContext.getResources().getString(R.string.relationship_status_parse), relationship);
        }

        public void setRelationship(String relationship) {
            this.relationship = relationship;
        }

        public String getEmployment(Context applicationContext) {
            return String.format(applicationContext.getResources().getString(R.string.employment_status_parse), employment);
        }

        public void setEmployment(String employment) {
            this.employment = employment;
        }

        public String getReligion(Context applicationContext) {
            return String.format(applicationContext.getResources().getString(R.string.religion_parse), religion);
        }

        public void setReligion(String religion) {
            this.religion = religion;
        }

        @SerializedName("blank")
        @Expose
        private Boolean isBlank;

        @SerializedName("sex")
        @Expose
        private String sex;

        @SerializedName("ethnicity")
        @Expose
        private ArrayList<String> ethnicity;

        @SerializedName("lang")
        @Expose
        private ArrayList<String> lang;

        @SerializedName("education")
        @Expose
        private String education;

        @SerializedName("relationship")
        @Expose
        private String relationship;

        @SerializedName("employment")
        @Expose
        private String employment;

        @SerializedName("religion")
        @Expose
        private String religion;
    }

    public class PreferencesInfoSettings {
        public Boolean getBlank() {
            return isBlank;
        }

        public void setBlank(Boolean blank) {
            isBlank = blank;
        }

        public String getSex(Context applicationContext) {
            return String.format(applicationContext.getResources().getString(R.string.questionnaires_that_only_target_my_sex), sex);
        }

        public void setSex(Boolean sex) {
            this.sex = sex;
        }

        public String getTranslated(Context applicationContext) {
            return String.format(applicationContext.getResources().getString(R.string.no_translated_questionnaires), translated);
        }

        public void setTranslated(Boolean translated) {
            this.translated = translated;
        }

        public String getCategories(Context applicationContext) {
            StringBuilder output = new StringBuilder();
            for (int i = 0; i < categories.size(); i++) {
                if (i+1 == categories.size()) {
                    output.append(categories.get(i)).append("");
                    continue;
                }
                output.append(categories.get(i)).append(", ");
            }
            return String.format(applicationContext.getResources().getString(R.string.categories_parse), output.toString());
        }

        public void setCategories(ArrayList<String> categories) {
            this.categories = categories;
        }

        public String getCompanies(Context applicationContext) {
            StringBuilder output = new StringBuilder();
            for (int i = 0; i < companies.size(); i++) {
                if (i+1 == companies.size()) {
                    output.append(companies.get(i)).append("");
                    continue;
                }
                output.append(companies.get(i)).append(", ");
            }
            return String.format(applicationContext.getResources().getString(R.string.companies_parse),output.toString());
        }

        public void setCompanies(ArrayList<String> companies) {
            this.companies = companies;
        }

        @SerializedName("blank")
        @Expose
        private Boolean isBlank;

        @SerializedName("sex")
        @Expose
        private Boolean sex;

        @SerializedName("translated")
        @Expose
        private Boolean translated;

        @SerializedName("categories")
        @Expose
        private ArrayList<String> categories;

        @SerializedName("companies")
        @Expose
        private ArrayList<String> companies;
    }

    public class PaymentInfoSettings {
        public Boolean getBlank() {
            return isBlank;
        }

        public void setBlank(Boolean blank) {
            isBlank = blank;
        }

        public Boolean getHasLabsMem() {
            return hasLabsMem;
        }

        public void setHasLabsMem(Boolean hasLabsMem) {
            this.hasLabsMem = hasLabsMem;
        }

        public long getLabs_expire() {
            //            SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy", context.getResources().getConfiguration().locale);
//            Date since = new Date(member_since);
//            return String.format(context.getResources().getString(R.string.member_since), format.format(since));
            return labs_expire;
        }

        public void setLabs_expire(int labs_expire) {
            this.labs_expire = labs_expire;
        }

        public int getLabs_price() {
            return labs_price;
        }

        public void setLabs_price(int labs_price) {
            this.labs_price = labs_price;
        }

        @SerializedName("blank")
        @Expose
        private Boolean isBlank;

        @SerializedName("labs_membership")
        @Expose
        private Boolean hasLabsMem;

        @SerializedName("labs_expire")
        @Expose
        private long labs_expire;

        @SerializedName("labs_price")
        @Expose
        private int labs_price;
    }
}
