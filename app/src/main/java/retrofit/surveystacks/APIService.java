package retrofit.surveystacks;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by SSDevTeam on 1/22/18.
 */

public interface APIService {

    //region POST
    @FormUrlEncoded
    @POST("/api/login")
    Call<SessionInit> login(
            @Field("AUTH_TOKEN") String auth_token,
            @Field("lang") String lang,
            @Field("username") String username,
            @Field("password") String password,
            @Field("webApp") Boolean webApp
    );

    @FormUrlEncoded
    @POST("/api/register")
    Call<SessionInit> register(
            @Field("AUTH_TOKEN") String auth_token,
            @Field("lang") String lang,
            @Field("fullname") String fullname,
            @Field("email_phone") String email_phone,
            @Field("username") String username,
            @Field("password") String password,
            @Field("dob") String dob,
            @Field("webApp") Boolean webApp
    );

    @FormUrlEncoded
    @POST("/api/forgot_username")
    Call<SessionInit> forgot_username(
            @Field("AUTH_TOKEN") String auth_token,
            @Field("lang") String lang,
            @Field("email_phone") String email_username,
            @Field("dob") String dob,
            @Field("webApp") Boolean webApp
    );

    @FormUrlEncoded
    @POST("/api/forgot_password")
    Call<SessionInit> forgot_password(
            @Field("AUTH_TOKEN") String auth_token,
            @Field("lang") String lang,
            @Field("username") String username,
            @Field("email_phone") String email_username,
            @Field("dob") String dob,
            @Field("webApp") Boolean webApp
    );

    @FormUrlEncoded
    @POST("/api/reset_password")
    Call<SessionInit> reset_password(
            @Field("AUTH_TOKEN") String auth_token,
            @Field("lang") String lang,
            @Field("username") String username,
            @Field("code") String code,
            @Field("password") String password,
            @Field("webApp") Boolean webApp
    );

    //endregion
    //region GET
    @Headers("X-Requested-With:XMLHttpRequest")
    @GET("/api/user/b0058c9ee07d3b5a49")
    Call<Session> sessionData(
            @Query("reference_code") String reference_code,
            @Query("key") String key,
            @Query("validation_key") String validation_key,
            @Query("auth_key") String auth_key,
            @Query("session_key") String session_key,
            @Query("lang") String lang,
            @Query("webApp") Boolean webApp
    );

    @Headers("X-Requested-With:XMLHttpRequest")
    @GET("/api/user/9e476d177153b44685")
    Call<Session> surveysData(
            @Query("reference_code") String reference_code,
            @Query("key") String key,
            @Query("validation_key") String validation_key,
            @Query("auth_key") String auth_key,
            @Query("session_key") String session_key,
            @Query("lang") String lang,
            @Query("webApp") Boolean webApp
    );

    @Headers("X-Requested-With:XMLHttpRequest")
    @GET("/api/user/1c18d221496174a6aa")
    Call<Session> pollsData(
            @Query("reference_code") String reference_code,
            @Query("key") String key,
            @Query("validation_key") String validation_key,
            @Query("auth_key") String auth_key,
            @Query("session_key") String session_key,
            @Query("lang") String lang,
            @Query("webApp") Boolean webApp
    );

    @Headers("X-Requested-With:XMLHttpRequest")
    @GET("/api/user/1acef401f0ac771d72")
    Call<Session> aboutData(
            @Query("reference_code") String reference_code,
            @Query("key") String key,
            @Query("validation_key") String validation_key,
            @Query("auth_key") String auth_key,
            @Query("session_key") String session_key,
            @Query("lang") String lang,
            @Query("webApp") Boolean webApp
    );

    @Headers("X-Requested-With:XMLHttpRequest")
    @GET("/api/user/a4fbedfaf5414d9795")
    Call<Session> paymentData(
            @Query("reference_code") String reference_code,
            @Query("key") String key,
            @Query("validation_key") String validation_key,
            @Query("auth_key") String auth_key,
            @Query("session_key") String session_key,
            @Query("lang") String lang,
            @Query("webApp") Boolean webApp
    );


    @Headers("X-Requested-With:XMLHttpRequest")
    @GET("/api/user/d61beb4fc45900f94d")
    Call<Profile> profileData(
            @Query("reference_code") String reference_code,
            @Query("key") String key,
            @Query("validation_key") String validation_key,
            @Query("auth_key") String auth_key,
            @Query("session_key") String session_key,
            @Query("lang") String lang,
            @Query("webApp") Boolean webApp
    );

    @Headers("X-Requested-With:XMLHttpRequest")
    @GET("/api/user/0357e6e320980d2d8a")
    Call<Session> rewardsData(
            @Query("reference_code") String reference_code,
            @Query("key") String key,
            @Query("validation_key") String validation_key,
            @Query("auth_key") String auth_key,
            @Query("session_key") String session_key,
            @Query("lang") String lang,
            @Query("webApp") Boolean webApp
    );

    @Headers("X-Requested-With:XMLHttpRequest")
    @GET("/api/user/4bc78765e700fc4c2b")
    Call<Session> managerData(
            @Query("reference_code") String reference_code,
            @Query("key") String key,
            @Query("validation_key") String validation_key,
            @Query("auth_key") String auth_key,
            @Query("session_key") String session_key,
            @Query("lang") String lang,
            @Query("webApp") Boolean webApp
    );

    @Headers("X-Requested-With:XMLHttpRequest")
    @GET("/api/user/d634cb4a34e7746fdc")
    Call<Session> bookmarksData(
            @Query("reference_code") String reference_code,
            @Query("key") String key,
            @Query("validation_key") String validation_key,
            @Query("auth_key") String auth_key,
            @Query("session_key") String session_key,
            @Query("lang") String lang,
            @Query("webApp") Boolean webApp
    );

    @Headers("X-Requested-With:XMLHttpRequest")
    @GET("/api/user/3ebf12a3d9917ba93b")
    Call<Session> labsData(
            @Query("reference_code") String reference_code,
            @Query("key") String key,
            @Query("validation_key") String validation_key,
            @Query("auth_key") String auth_key,
            @Query("session_key") String session_key,
            @Query("lang") String lang,
            @Query("webApp") Boolean webApp
    );

    @Headers("X-Requested-With:XMLHttpRequest")
    @GET("/api/user/afdc2625c2facd6326")
    Call<Session> editorData(
            @Query("reference_code") String reference_code,
            @Query("key") String key,
            @Query("validation_key") String validation_key,
            @Query("auth_key") String auth_key,
            @Query("session_key") String session_key,
            @Query("lang") String lang,
            @Query("type") String type,
            @Query("token") String token,
            @Query("webApp") Boolean webApp
    );

    @GET("/api/user/0695d378ae2144f4d4")
    Call<Session> receiptData(
            @Query("reference_code") String reference_code,
            @Query("key") String key,
            @Query("validation_key") String validation_key,
            @Query("auth_key") String auth_key,
            @Query("session_key") String session_key,
            @Query("lang") String lang,
            @Query("type") String type,
            @Query("token") String token,
            @Query("webApp") Boolean webApp
    );

    @Headers("X-Requested-With:XMLHttpRequest")
    @GET("/api/user/75076262024E2A793D")
    Call<Settings> settingsData(
            @Query("reference_code") String reference_code,
            @Query("key") String key,
            @Query("validation_key") String validation_key,
            @Query("auth_key") String auth_key,
            @Query("session_key") String session_key,
            @Query("lang") String lang,
            @Query("webApp") Boolean webApp
    );

    @Headers("X-Requested-With:XMLHttpRequest")
    @GET("/api/user/41562427f26404d237")
    Call<Session> analyticsData(
            @Query("reference_code") String reference_code,
            @Query("key") String key,
            @Query("validation_key") String validation_key,
            @Query("auth_key") String auth_key,
            @Query("session_key") String session_key,
            @Query("lang") String lang,
            @Query("webApp") Boolean webApp
    );

    @Headers("X-Requested-With:XMLHttpRequest")
    @GET("/api/user/00b169f453b6d34063")
    Call<Session> teamsData(
            @Query("reference_code") String reference_code,
            @Query("key") String key,
            @Query("validation_key") String validation_key,
            @Query("auth_key") String auth_key,
            @Query("session_key") String session_key,
            @Query("lang") String lang,
            @Query("webApp") Boolean webApp
    );

    @Headers("X-Requested-With:XMLHttpRequest")
    @GET("/api/user/8f5f974a5fda1c42fb")
    Call<Session> reportData(
            @Query("reference_code") String reference_code,
            @Query("key") String key,
            @Query("validation_key") String validation_key,
            @Query("auth_key") String auth_key,
            @Query("session_key") String session_key,
            @Query("lang") String lang,
            @Query("type") String type,
            @Query("token") String token,
            @Query("webApp") Boolean webApp
    );

    @Headers("X-Requested-With:XMLHttpRequest")
    @GET("/api/user/6c25ce5369fc7c0a5b")
    Call<Session> preferencesData(
            @Query("reference_code") String reference_code,
            @Query("key") String key,
            @Query("validation_key") String validation_key,
            @Query("auth_key") String auth_key,
            @Query("session_key") String session_key,
            @Query("lang") String lang,
            @Query("webApp") Boolean webApp
    );

    @Headers("X-Requested-With:XMLHttpRequest")
    @GET("/api/user/c3789b05570894bcfd")
    Call<Session> pollAnswerData(
            @Query("reference_code") String reference_code,
            @Query("key") String key,
            @Query("validation_key") String validation_key,
            @Query("auth_key") String auth_key,
            @Query("session_key") String session_key,
            @Query("lang") String lang,
            @Query("token") String token,
            @Query("webApp") Boolean webApp
    );

    @Headers("X-Requested-With:XMLHttpRequest")
    @GET("/api/user/5a239f4c0ec530c497")
    Call<Session> surveyAnswerData(
            @Query("reference_code") String reference_code,
            @Query("key") String key,
            @Query("validation_key") String validation_key,
            @Query("auth_key") String auth_key,
            @Query("session_key") String session_key,
            @Query("lang") String lang,
            @Query("token") String token,
            @Query("webApp") Boolean webApp
    );

    @Headers("X-Requested-With:XMLHttpRequest")
    @GET("/api/user/f02d69d2b65a3a17f4")
    Call<Session> searchData(
            @Query("reference_code") String reference_code,
            @Query("key") String key,
            @Query("validation_key") String validation_key,
            @Query("auth_key") String auth_key,
            @Query("session_key") String session_key,
            @Query("lang") String lang,
            @Query("searchQuery") String query,
            @Query("searchConstraint") String constraint,
            @Query("webApp") Boolean webApp
    );

    @Headers("X-Requested-With:XMLHttpRequest")
    @GET("/api/4868b0b4105908817f")
    Call<Session> categoriesData(
            @Query("reference_code") String reference_code,
            @Query("key") String key,
            @Query("lang") String lang,
            @Query("webApp") Boolean webApp
    );

    @Headers("X-Requested-With:XMLHttpRequest")
    @GET("/api/notifications/e1760a7b88184d0517f11c900342e1c4fba47fdd")
    Call<Session> notificationsData(
            @Query("reference_code") String reference_code,
            @Query("key") String key,
            @Query("session_key") String session_key,
            @Query("lang") String lang,
            @Query("webApp") Boolean webApp
    );

    @Headers("X-Requested-With:XMLHttpRequest")
    @GET("/api/b574be1ff5d0b12f2f")
    Call<Session> creatorData(
            @Query("reference_code") String reference_code,
            @Query("key") String key,
            @Query("lang") String lang,
            @Query("webApp") Boolean webApp
    );
    //endregion
}
