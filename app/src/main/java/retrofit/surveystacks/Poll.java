package retrofit.surveystacks;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Poll {
    @SerializedName("answer_url")
    @Expose
    private String answer_url;

    @SerializedName("bookmarked")
    @Expose
    private Boolean bookmarked;

    @SerializedName("creator")
    @Expose
    private String creator;

    @SerializedName("creator_image_url")
    @Expose
    private String creator_image_url;

    @SerializedName("image_url")
    @Expose
    private String image_url;

    @SerializedName("questions")
    @Expose
    private int questions;

    @SerializedName("coins")
    @Expose
    private int coins;

    @SerializedName("surveyor_id")
    @Expose
    private String surveyor_id;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("token")
    @Expose
    private String token;

    String getAnswer_url() {
        return answer_url;
    }

    public void setAnswer_url(String answer_url) {
        this.answer_url = answer_url;
    }

    String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    String getCreator_image_url() {
        return creator_image_url;
    }

    public void setCreator_image_url(String creator_image_url) {
        this.creator_image_url = creator_image_url;
    }

    String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getSurveyor_id() {
        return surveyor_id;
    }

    public void setSurveyor_id(String surveyor_id) {
        this.surveyor_id = surveyor_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Boolean getBookmarked() {
        return bookmarked;
    }

    public void setBookmarked(Boolean bookmarked) {
        this.bookmarked = bookmarked;
    }

    public int getQuestions() {
        return questions;
    }

    public void setQuestions(int questions) {
        this.questions = questions;
    }

    String getCoins() {
        return String.valueOf(coins);
    }

    public void setCoins(int coins) {
        this.coins = coins;
    }
}